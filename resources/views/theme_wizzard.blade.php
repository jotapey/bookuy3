@extends('layouts.admin')

@section ('content')
<section class="content">
    <!--main content-->
    <div class="row">
        <div class="col-md-4">
                        
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="signal" data-size="16" data-loop="true" data-c="#fff" data-hc="white" id="livicon-47" style="width: 16px; height: 16px;"><svg height="16" version="1.1" width="16" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; left: -0.800018px; top: -0.800018px;" id="canvas-for-livicon-47"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="#ffffff" stroke="none" d="M6,27.5C6,27.781,5.781,28,5.5,28H2.5C2.219,28,2,27.781,2,27.5V24.5C2,24.219,2.219,24,2.5,24H5.5C5.781,24,6,24.219,6,24.5V27.5ZM12,27.5C12,27.781,11.781,28,11.5,28H8.5C8.219,28,8,27.781,8,27.5V22.5C8,22.219,8.219,22,8.5,22H11.5C11.781,22,12,22.219,12,22.5V27.5ZM18,27.5C18,27.781,17.781,28,17.5,28H14.5C14.219,28,14,27.781,14,27.5V18.5C14,18.219,14.219,18,14.5,18H17.5C17.781,18,18,18.219,18,18.5V27.5Z" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="#ffffff" stroke="none" d="M24,27.5C24,27.781,23.781,28,23.5,28H20.5C20.219,28,20,27.781,20,27.5V12.5C20,12.219,20.219,12,20.5,12H23.5C23.781,12,24,12.219,24,12.5V27.5Z" stroke-width="0" opacity="1" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path><path fill="#ffffff" stroke="none" d="M30,27.5C30,27.781,29.781,28,29.5,28H26.5C26.219,28,26,27.781,26,27.5V4.5C26,4.219,26.219,4,26.5,4H29.5C29.781,4,30,4.219,30,4.5V27.5Z" stroke-width="0" opacity="1" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path></svg></i>
                        Default Accordions
                    </h3>
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>
                </div>
                <div class="panel-body"><div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                                    <h4 class="panel-title">Accordion1</h4>
                                </a>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title">
                                    Accordion2
                                        </h4>
                                </a>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                                <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                    <h4 class="panel-title"> Accordion3</h4>
                                </a>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree" aria-expanded="true">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- nav-tabs-custom -->
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="signal" data-size="16" data-loop="true" data-c="#fff" data-hc="white" id="livicon-47" style="width: 16px; height: 16px;"><svg height="16" version="1.1" width="16" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; left: -0.800018px; top: -0.800018px;" id="canvas-for-livicon-47"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="#ffffff" stroke="none" d="M6,27.5C6,27.781,5.781,28,5.5,28H2.5C2.219,28,2,27.781,2,27.5V24.5C2,24.219,2.219,24,2.5,24H5.5C5.781,24,6,24.219,6,24.5V27.5ZM12,27.5C12,27.781,11.781,28,11.5,28H8.5C8.219,28,8,27.781,8,27.5V22.5C8,22.219,8.219,22,8.5,22H11.5C11.781,22,12,22.219,12,22.5V27.5ZM18,27.5C18,27.781,17.781,28,17.5,28H14.5C14.219,28,14,27.781,14,27.5V18.5C14,18.219,14.219,18,14.5,18H17.5C17.781,18,18,18.219,18,18.5V27.5Z" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="#ffffff" stroke="none" d="M24,27.5C24,27.781,23.781,28,23.5,28H20.5C20.219,28,20,27.781,20,27.5V12.5C20,12.219,20.219,12,20.5,12H23.5C23.781,12,24,12.219,24,12.5V27.5Z" stroke-width="0" opacity="1" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path><path fill="#ffffff" stroke="none" d="M30,27.5C30,27.781,29.781,28,29.5,28H26.5C26.219,28,26,27.781,26,27.5V4.5C26,4.219,26.219,4,26.5,4H29.5C29.781,4,30,4.219,30,4.5V27.5Z" stroke-width="0" opacity="1" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path></svg></i>
                        Default Accordions
                    </h3>
                </div>
                    <div class="panel-body">
                        <div class="panel-group">
                            as
                        </div>
                    </div>
            </div>

        </div>
        <!--col-md-6 ends-->
        <!--col-md-6 ends-->
        <div class="col-md-8">
            <div class="panel panel-warning" id="hidepanel5">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="gift" data-size="16" data-loop="true" data-c="#fff" data-hc="white" id="livicon-48" style="width: 16px; height: 16px;"><svg height="16" version="1.1" width="16" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative; left: -0.262512px; top: -0.800003px;" id="canvas-for-livicon-48"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="#ffffff" stroke="none" d="M20,8.542C20,7.669,21,7.592,21,7.011C21,6.852,20.922,6.640000000000001,20.853,6.501L20.860000000000003,6.503L20.857,6.5C21.473,6.79,22,7.188,22,7.958C22,8.831,21,8.908,21,9.49C21,9.648,21.078,9.861,21.15,10L21.139999999999997,9.998L21.143,10C20.527,9.711,20,9.312,20,8.542ZM11.143,10L11.141,9.998L11.15,10C11.078,9.861,11,9.649,11,9.49C11,8.909,12,8.831,12,7.9590000000000005C12,7.189,11.473,6.790000000000001,10.857,6.501L10.859,6.503L10.853,6.5C10.922,6.639,11,6.851,11,7.01C11,7.592,10,7.669,10,8.541C10,9.312,10.527,9.71,11.143,10ZM16.143,9L16.141000000000002,8.998L16.15,9C16.078,8.861,16,8.649,16,8.49C16,7.909000000000001,17,7.831,17,6.9590000000000005C17,6.189,16.473,5.790000000000001,15.857,5.501L15.859,5.503L15.853,5.5C15.922,5.639,16,5.851,16,6.01C16,6.592,15,6.669,15,7.5409999999999995C15,8.312,15.527,8.71,16.143,9ZM27,17V24C27,26.209,22.075,28,16,28S5,26.209,5,24V17C5,15.597,6.992,14.364,10,13.65V11.438C10,11.196,10.224,11,10.5,11H11.5C11.776,11,12,11.196,12,11.438V13.277000000000001C12.945,13.143,13.95,13.053,15,13.019000000000002V10.438000000000002C15,10.196,15.224,10,15.5,10H16.5C16.775,10,17,10.196,17,10.438V13.019C18.051,13.053,19.056,13.143,20,13.277000000000001V11.438C20,11.196,20.224,11,20.501,11H21.5C21.777,11,22,11.196,22,11.438V13.65C25.008,14.364,27,15.597,27,17ZM25,17C25,16.26,23.837,15.598,22,15.14V17.562C22,17.805,21.777,18,21.5,18H20.501C20.224,18,20,17.805,20,17.563V14.762999999999998C19.076999999999998,14.635999999999997,18.069,14.548999999999998,17,14.515999999999998V16.563C17,16.805,16.775,17,16.5,17H15.5C15.224,17,15,16.805,15,16.563V14.515999999999998C13.931000000000001,14.548999999999998,12.922,14.634999999999998,12,14.762999999999998V17.563C12,17.805,11.776,18,11.5,18H10.5C10.224,18,10,17.805,10,17.562V15.14C8.163,15.598,7,16.26,7,17C7,18.381,11.029,19.5,16,19.5S25,18.381,25,17Z" opacity="0" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#ffffff" stroke="none" d="M5.199,30C4.537,30,4,29.418,4,28.701V17H15V30H5.199ZM28,28.701V17H17V30H26.801000000000002C27.463,30,28,29.418,28,28.701Z" opacity="1" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path><path fill="#ffffff" stroke="none" d="M11.508,2.353C10.597999999999999,1.9410000000000003,8.748,1.7460000000000002,7.507999999999999,2.353C6.187999999999999,3,5.124999999999999,4.625,5.507999999999999,6.353C6.115,9.083,10,10,10,10H3.199C2.537,10,2,10.537,2,11.2V16H15V10H16C16,10,14.688,3.79,11.508,2.353ZM9.908,8.263C9.103,8.099,7.702999999999999,7.743,7.359,6.193C7.142,5.212999999999999,7.744,4.2909999999999995,8.494,3.9229999999999996C9.197,3.5779999999999994,10.248,3.6899999999999995,10.765,3.9229999999999996C12.568000000000001,4.736999999999999,13.314,8.263,13.314,8.263S11.375,8.563,9.908,8.263ZM28.801,10H22C22,10,25.884999999999998,9.083,26.492,6.353C26.875,4.625,25.812,2.9999999999999996,24.492,2.3529999999999998C23.252000000000002,1.7459999999999998,21.402,1.9409999999999998,20.492,2.3529999999999998C17.313,3.79,16,10,16,10H17V16H30V11.2C30,10.537,29.463,10,28.801,10ZM18.686,8.263C18.686,8.263,19.432,4.7379999999999995,21.235,3.923C21.753,3.69,22.803,3.5780000000000003,23.506,3.923C24.256,4.29,24.858,5.212,24.641000000000002,6.193C24.297,7.744,22.897000000000002,8.099,22.092000000000002,8.263C20.625,8.563,18.686,8.263,18.686,8.263Z" opacity="1" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path></svg><svg height="16" version="1.1" width="16" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; left: -0.800018px; top: -0.200012px;" id="canvas-for-livicon-48"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="#ffffff" stroke="none" d="M20,8.542C20,7.669,21,7.592,21,7.011C21,6.852,20.922,6.640000000000001,20.853,6.501L20.860000000000003,6.503L20.857,6.5C21.473,6.79,22,7.188,22,7.958C22,8.831,21,8.908,21,9.49C21,9.648,21.078,9.861,21.15,10L21.139999999999997,9.998L21.143,10C20.527,9.711,20,9.312,20,8.542ZM11.143,10L11.141,9.998L11.15,10C11.078,9.861,11,9.649,11,9.49C11,8.909,12,8.831,12,7.9590000000000005C12,7.189,11.473,6.790000000000001,10.857,6.501L10.859,6.503L10.853,6.5C10.922,6.639,11,6.851,11,7.01C11,7.592,10,7.669,10,8.541C10,9.312,10.527,9.71,11.143,10ZM16.143,9L16.141000000000002,8.998L16.15,9C16.078,8.861,16,8.649,16,8.49C16,7.909000000000001,17,7.831,17,6.9590000000000005C17,6.189,16.473,5.790000000000001,15.857,5.501L15.859,5.503L15.853,5.5C15.922,5.639,16,5.851,16,6.01C16,6.592,15,6.669,15,7.5409999999999995C15,8.312,15.527,8.71,16.143,9ZM27,17V24C27,26.209,22.075,28,16,28S5,26.209,5,24V17C5,15.597,6.992,14.364,10,13.65V11.438C10,11.196,10.224,11,10.5,11H11.5C11.776,11,12,11.196,12,11.438V13.277000000000001C12.945,13.143,13.95,13.053,15,13.019000000000002V10.438000000000002C15,10.196,15.224,10,15.5,10H16.5C16.775,10,17,10.196,17,10.438V13.019C18.051,13.053,19.056,13.143,20,13.277000000000001V11.438C20,11.196,20.224,11,20.501,11H21.5C21.777,11,22,11.196,22,11.438V13.65C25.008,14.364,27,15.597,27,17ZM25,17C25,16.26,23.837,15.598,22,15.14V17.562C22,17.805,21.777,18,21.5,18H20.501C20.224,18,20,17.805,20,17.563V14.762999999999998C19.076999999999998,14.635999999999997,18.069,14.548999999999998,17,14.515999999999998V16.563C17,16.805,16.775,17,16.5,17H15.5C15.224,17,15,16.805,15,16.563V14.515999999999998C13.931000000000001,14.548999999999998,12.922,14.634999999999998,12,14.762999999999998V17.563C12,17.805,11.776,18,11.5,18H10.5C10.224,18,10,17.805,10,17.562V15.14C8.163,15.598,7,16.26,7,17C7,18.381,11.029,19.5,16,19.5S25,18.381,25,17Z" opacity="0" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#ffffff" stroke="none" d="M5.199,30C4.537,30,4,29.418,4,28.701V17H15V30H5.199ZM28,28.701V17H17V30H26.801000000000002C27.463,30,28,29.418,28,28.701Z" opacity="1" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path><path fill="#ffffff" stroke="none" d="M11.508,2.353C10.597999999999999,1.9410000000000003,8.748,1.7460000000000002,7.507999999999999,2.353C6.187999999999999,3,5.124999999999999,4.625,5.507999999999999,6.353C6.115,9.083,10,10,10,10H3.199C2.537,10,2,10.537,2,11.2V16H15V10H16C16,10,14.688,3.79,11.508,2.353ZM9.908,8.263C9.103,8.099,7.702999999999999,7.743,7.359,6.193C7.142,5.212999999999999,7.744,4.2909999999999995,8.494,3.9229999999999996C9.197,3.5779999999999994,10.248,3.6899999999999995,10.765,3.9229999999999996C12.568000000000001,4.736999999999999,13.314,8.263,13.314,8.263S11.375,8.563,9.908,8.263ZM28.801,10H22C22,10,25.884999999999998,9.083,26.492,6.353C26.875,4.625,25.812,2.9999999999999996,24.492,2.3529999999999998C23.252000000000002,1.7459999999999998,21.402,1.9409999999999998,20.492,2.3529999999999998C17.313,3.79,16,10,16,10H17V16H30V11.2C30,10.537,29.463,10,28.801,10ZM18.686,8.263C18.686,8.263,19.432,4.7379999999999995,21.235,3.923C21.753,3.69,22.803,3.5780000000000003,23.506,3.923C24.256,4.29,24.858,5.212,24.641000000000002,6.193C24.297,7.744,22.897000000000002,8.099,22.092000000000002,8.263C20.625,8.563,18.686,8.263,18.686,8.263Z" opacity="1" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path></svg></i>
                        Form Inputs
                    </h3>
                    <span class="pull-right">
                        <i class="glyphicon glyphicon-chevron-up clickable"></i>
                        <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                    </span>
                </div>
                <div class="panel-body">
                    <input type="text" class="styles-theme" />.
                    
                    <div class="bs-example">
                                    <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                                        <li class="active">
                                            <a href="#preview" data-toggle="tab" aria-expanded="true">Preview</a>
                                        </li>
                                        <li class="">
                                            <a href="#html" data-toggle="tab" aria-expanded="false">Html</a>
                                        </li>
                                        <li class="">
                                            <a href="#css" data-toggle="tab" aria-expanded="false">Css</a>
                                        </li>
                                    </ul>
                                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 130px;"><div id="myTabContent" class="tab-content" style="overflow: hidden; width: auto; height: 130px;">
                                        <div class="tab-pane fade in active" id="preview">
                                            <form role="form">
                                                <div class="fileinput fileinput-exists" data-provides="fileinput">
                                                    <input id="name1" name="name" type="text" placeholder="Enter your Email" class="form-control" style="position: absolute;width: 29%;z-index: 2;">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px;height: 150px;position: absolute;line-height: 150px;z-index: 1;"></div>
                                                    <div>
                                                        <span class="btn btn-default btn-file">
                                                            <span class="fileinput-new">Select image</span>
                                                            <span class="fileinput-exists">Change</span>
                                                            <input type="hidden" value="" name="..."><input type="hidden" value="" name=""><input type="file" name=""></span>
                                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade " id="html">
                                            <textarea class="form-control"></textarea>
                                        </div>
                                        <div class="tab-pane fade" id="css">
                                            <textarea class="form-control"></textarea>
                                        </div>
                                    </div><div class="slimScrollBar" style="background: rgb(216, 74, 56); width: 3px; position: absolute; top: 0px; opacity: 1; display: none; border-radius: 0px; z-index: 99; right: 1px; height: 112.667px;"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 0px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                                </div>

                </div>
            </div>
        </div>
        <!--col-md-6 ends-->
    </div>
    <!--main content ends-->
</section>
<style>
.btn-file{
    display:none;
}
.nav-tabs {
    border-bottom: 1px solid #ddd;
     float: none !important;
}
.styles-theme{
    width: 25%;
}
#myTabContent, .slimScrollDiv{
    height: 550px !important;
}
</style>
<script>
var data = [{ id: 0, text: 'enhancement' }, { id: 1, text: 'bug' }, { id: 2, text: 'duplicate' }, { id: 3, text: 'invalid' }, { id: 4, text: 'wontfix' }];

$(".styles-theme").select2({
  data: data
})

function showHtml(){
    ht = $('#preview').html();
    console.log(ht);
    var str = $("#html textarea").text(ht);
    console.log(str);
}

jQuery(document).ready(function(){
    
});
</script>
@endsection
