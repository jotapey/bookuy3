@extends('layouts.app')
@section('content')

    <!-- Editor Styles -->
    <link href="{{ asset('site_themes/'.Config::get('siteconfig.theme').'/css/editor.css') }}" rel="stylesheet">


    <!-- Drop Area Styles -->
    <link href="{{ asset('site_themes/'.Config::get('siteconfig.theme').'/css/droparea.css') }}" rel="stylesheet">
    <link href="{{ asset('general_assets/js/OwlCarousel2-2.2.1/dist/assets/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('general_assets/js/OwlCarousel2-2.2.1/dist/assets/owl.theme.default.css') }}" rel="stylesheet">


    <!-- Scripts -->
    <script>
        window.Laravel = {"csrfToken":"TcFfW1ijIwI47uw3MhFzAVZBmOlStwJVjpit0ptg"}
        var current_user = '<?php echo $user_id ?>';

        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })

    </script>
    <div id="app">

        <input type="hidden" id="base_url" value="{{ asset('') }}">
        <div class="container-fluid">
            <div class="row">
                @include("editor/components/selectorstyles")
                @include("editor/components/workarea/workarea")
                @include("editor/components/previewpages")

            </div>
        </div>
    </div>

    <style>
        /* Loading*/

        #loading-img {
            background: url(http://preloaders.net/preloaders/360/Velocity.gif) center center no-repeat;
            height: 100%;
            z-index: 20;
        }

        .overlay {
            background: #e9e9e9;
            display: none;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            opacity: 0.5;
			height:100%;
        }

        /* End Loading */
    </style>

    <!-- Drop image area -->
    <script src="{{ asset('site_themes/'.Config::get('siteconfig.theme').'/js/droparea.js') }}"></script>

    <script>
        var editor = true;
    </script>
    @include("editor/components/modal")
    @include("editor/components/modal_images_source")
    @include("editor/components/modal_gallery")
    @include("editor/components/modal_audio")
    @include("editor/components/modal_item")



    <!-- Drop image area -->

    <script type="text/javascript" src="{{ asset('general_assets/js/OwlCarousel2-2.2.1/dist/owl.carousel.js') }}"></script>

    <script type="text/javascript" src="{{ asset('general_assets/js/cropit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('general_assets/js/html2canvas.js') }}"></script>
    <script type="text/javascript" src="{{ asset('general_assets/js/rasterizeHTML.allinone.js') }}"></script>
    <script type="text/javascript" src="{{ asset('general_assets/js/jquery.upload_preview.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('general_assets/js/validate.js') }}"></script>

    <script type="text/javascript" src="{{ asset('pages/editor/global_objects.js') }}"></script>
    <script type="text/javascript" src="{{ asset('pages/editor/global_functions.js') }}"></script>
    <script type="text/javascript" src="{{ asset('pages/editor/global_events.js') }}"></script>
    <script type="text/javascript" src="{{ asset('pages/editor/audio.js') }}"></script>

    <script type="text/javascript" src="{{ asset('pages/editor/modals/cover_modal/functions.js') }}"></script>
    <script type="text/javascript" src="{{ asset('pages/editor/modals/cover_modal/events.js') }}"></script>

    <script type="text/javascript" src="{{ asset('pages/editor/modals/items_modal/functions.js') }}"></script>
    <script type="text/javascript" src="{{ asset('pages/editor/modals/items_modal/events.js') }}"></script>

    <script type="text/javascript" src="{{ asset('pages/editor/partials/items_carrousell/functions.js') }}"></script>
    <script type="text/javascript" src="{{ asset('pages/editor/partials/items_carrousell/events.js') }}"></script>

    <script>
        var audio_files = [];
        <?php if($book_active):?>
        var tmp_book_active = JSON.parse('<?php echo json_encode($book_active) ?>');
        book_active = {
            id:tmp_book_active.id,
            author_id:tmp_book_active.author_id,
            status:tmp_book_active.status,
            title:tmp_book_active.title,
            author_name:tmp_book_active.author_name,
            ilustrador_name:tmp_book_active.ilustrador_name,
            css:tmp_book_active.css
        };
        t_css = book_active.css;
        /*for(var i in t_css){
            jQuery("#loaded-css-links").append("<link rel='stylesheet' href='" +  t_css[i] + "' >");
        }*/
		for(var i in t_css){
			c = t_css[i];
			if(c != "undefined"){
                if(c.indexOf("../")!== -1){
                    if(c.indexOf("http:")=== -1){
                        c = "../"+c;
                    }
				}
				jQuery("#loaded-css-links").append("<link rel='stylesheet' href='" +  c + "' >");
			}
        }
        <?php endif; ?>

        <?php if(count($loaded_items)):?>
        <?php $items = $loaded_items; ?>
        jQuery("#myModal").modal("hide");
        try{
            var items = JSON.parse('<?php echo json_encode($items) ?>');
        }catch(e){
            var str = '<?php echo json_encode($items) ?>';
            var find = '\/';
            var re = new RegExp(find, 'g');
            str = str.replace(re, '/');

            str = str.replace(/\\/g, '/');

            var items = JSON.parse(str);
        }
        var div = jQuery(".panel-body-horizontal");
        var span = "";
        for(var i in items){
            span += '<div class="canvas-container">';
            span += '<canvas width="800" height="400" id="thumbnail_cover" type="'+items[i].type+'" item_id="'+items[i].id+'" url_source="'+items[i].thumbnail+'" book_id="'+items[i].book_id+'" class="" type="page"></canvas>'
            span += '<button class="eliminar-canvas">x</button>';
            span += '</div>';
        }

        div.html(span);

        jQuery(".panel-body-horizontal").find("canvas").each(function(){
            imageToCanvas(jQuery(this), jQuery(this).attr("url_source"));
        })

        function imageToCanvas(myCanvas,url){
            var ctx = myCanvas[0].getContext('2d');
            var img = new Image;
            img.src = url;
            img.onload = function(){
                ctx.drawImage(img,0,0); // Or at whatever offset you like
            };
        }

        /*jQuery(document).on("click", "canvas", function(){
            jQuery(document).find("canvas.selected").removeClass("selected");
            jQuery(this).addClass("selected");

            selected_item = {
                type:jQuery(this).attr("type"),
                book_id:jQuery(this).attr("book_id"),
                item_id:jQuery(this).attr("item_id")
            }

            loadHtmlItemSaved();

        });*/
        jQuery(".panel-body-horizontal canvas:first").click();
        <?php endif;?>
        jQuery(document).ready(function(){
            <?php if($book_active):?>
            jQuery(".overlay").show();
            <?php endif;?>
        })
    </script>

    <div class="overlay">
        <div id="loading-img"></div>
    </div>
    <!--No responsive-->
    <style>
        body {  padding-bottom: 30px; } /* Finesse the page header spacing */ .page-header { margin-bottom: 30px; } .page-header .lead { margin-bottom: 10px; } /* Non-responsive overrides * * Utilitze the following CSS to disable the responsive-ness of the container, * grid system, and navbar. */ /* Reset the container */ .container { width: 970px; max-width: none !important; } /* Demonstrate the grids */ .col-xs-4 { padding-top: 15px; padding-bottom: 15px; background-color: #eee; background-color: rgba(86,61,124,.15); border: 1px solid #ddd; border: 1px solid rgba(86,61,124,.2); } .container .navbar-header, .container .navbar-collapse { margin-right: 0; margin-left: 0; } /* Always float the navbar header */ .navbar-header { float: left; } /* Undo the collapsing navbar */ .navbar-collapse { display: block !important; height: auto !important; padding-bottom: 0; overflow: visible !important; } .navbar-toggle { display: none; } .navbar-collapse { border-top: 0; } .navbar-brand { margin-left: -15px; } /* Always apply the floated nav */ .navbar-nav { float: left; margin: 0; } .navbar-nav > li { float: left; } .navbar-nav > li > a { padding: 15px; } /* Redeclare since we override the float above */ .navbar-nav.navbar-right { float: right; } /* Undo custom dropdowns */ .navbar .navbar-nav .open .dropdown-menu { position: absolute; float: left; background-color: #fff; border: 1px solid #ccc; border: 1px solid rgba(0, 0, 0, .15); border-width: 0 1px 1px; border-radius: 0 0 4px 4px; -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175); box-shadow: 0 6px 12px rgba(0, 0, 0, .175); } .navbar-default .navbar-nav .open .dropdown-menu > li > a { color: #333; } .navbar .navbar-nav .open .dropdown-menu > li > a:hover, .navbar .navbar-nav .open .dropdown-menu > li > a:focus, .navbar .navbar-nav .open .dropdown-menu > .active > a, .navbar .navbar-nav .open .dropdown-menu > .active > a:hover, .navbar .navbar-nav .open .dropdown-menu > .active > a:focus { color: #fff !important; background-color: #428bca !important; } .navbar .navbar-nav .open .dropdown-menu > .disabled > a, .navbar .navbar-nav .open .dropdown-menu > .disabled > a:hover, .navbar .navbar-nav .open .dropdown-menu > .disabled > a:focus { color: #999 !important; background-color: transparent !important; }
        .image-editor{max-width: 800px;}

        .cover-inputs.form-group {
            position: relative;
            width: 100%;
            margin-top: 50px;
            margin-left: 15%;
            z-index: 1;
            float: left;
        }
        .tab-content .form-control {
            width: 75%;
        }
        .s100_percent_important, .cropit-preview-image-container{
                width: 100% !important;
        }

    </style>
    <script>
        var viewport = document.querySelector('meta[name="viewport"]');
        var cntMaxLength = 150;

        if(detectmob()){
            if ( viewport ) {
                viewport.content = "initial-scale=0.1";
                viewport.content = "width=950";
            }
        }

        function detectmob() {
            if( navigator.userAgent.match(/Android/i)
                || navigator.userAgent.match(/webOS/i)
                || navigator.userAgent.match(/iPhone/i)
                || navigator.userAgent.match(/iPad/i)
                || navigator.userAgent.match(/iPod/i)
                || navigator.userAgent.match(/BlackBerry/i)
                || navigator.userAgent.match(/Windows Phone/i)
            ){
                return true;
            }
            else {
                return false;
            }
        }

        jQuery(document).ready(function() {
            jQuery(".owl-carousel").owlCarousel({
                autoWidth:true
            });

        });

    </script>
    <style>
        ul.panel-body-horizontal{
            width:2000px;
        }
        .pages-preview-panel{
            overflow-x: scroll;
        }
        li.canvas-container{
            list-style: none;
        }
        .image-editor-wapper-page .cropit-preview-image-container{
            width: 360px !important;
            margin-left: 20px;
            margin-top: 20px;
            height: 360px !important;
        }
        .image-editor-wapper-cover .cropit-preview-image-container{
            width: 760px !important;
            margin-left: 20px;
            margin-top: 20px;
            height: 360px !important;
        }
    </style>
@endsection