<!-- Modal -->
<div class="modal fade" id="imagesSourceModal" tabindex="-1" role="dialog" aria-labelledby="imagesSourceModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="imagesSourceModalLabel">Subir imagen</h4>
            </div>
            <div class="modal-body">
                <button type="button" class="btn btn-default" data-dismiss="modal">Desde Galeria</button>
                <button type="button" class="btn btn-primary">Desde Archivo</button>
            </div>
        </div>
    </div>
</div>
<style>

    .modal-dialog .panel-body-main-vertical img {
        width: 100%;
    }

    .modal-dialog .panel-body-main-vertical label{
    }
    .modal-dialog .panel-collapse .panel-body {
        overflow-y: scroll;
    }
    .styles-preview-select {
        float: left;
        width: 49%;
        margin-right: 1%;
    }
</style>