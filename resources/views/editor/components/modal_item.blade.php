<!-- Modal -->
<div class="modal fade" id="modalItem" tabindex="-1" role="dialog" aria-labelledby="modalItemLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalItemLabel">Diseño General</h4>
            </div>
            <div class="modal-body" id="step1">
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body panel-body-main-vertical">
                        <div class="styles-group">
                            <div class="form-group themes">
                                <label style="text-align: left;">Diseño</label>
                                @foreach ($items as $item)
                                    @foreach ($item as $i)
                                        @if($i["type"] == "img")
                                        <a item_id="{{$i['name']}}" href="#" class="selector-item selector-item-page">
                                            <img src="{{url('').'/book_themes/pages/'.$i['name'].'/'.$i['path']}}" />

                                            <label>{{$i["name"]}}</label>
                                        </a>
                                        @endif
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-save-item">Guardar</button>
            </div>
        </div>
    </div>
</div>
<script>
    /*jQuery("#modalItem").modal({
        backdrop: 'static',
        keyboard: false
    });*/
</script>
