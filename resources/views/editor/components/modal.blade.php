<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Diseño General</h4>
            </div>
            <div class="modal-body" id="step1">
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body panel-body-main-vertical">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Titulo" name="book-title">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Autor" name="book-author">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Ilustrador" name="book-ilustrador">
                        </div>
                        <div class="styles-group">
                            <div class="form-group">
                                <label style="text-align: left;">Diseño</label>
                                @foreach ($paginas as $pagina)
                                    @foreach ($pagina as $item)
                                        @if($item["type"] == "img")
                                        <a
                                            item_id="{{$item['name']}}"
                                            href="#" class="selector-item selector-item-cover"
                                            @if($item["type"] == "css")
                                                css_path = "{{url('').'/book_themes/covers/'.$item['name'].'/'.$item['path']}}"
                                            @endif
                                        >
                                            <img src="{{url('').'/book_themes/covers/'.$item['name'].'/'.$item['path']}}" />
                                            <label>{{$item["name"]}}</label>
                                        </a>
                                        @endif
                                        @if($item["type"] == "css")
                                            <div class="cover_css_path" css_path = "{{url('').'/book_themes/covers/'.$item['name'].'/'.$item['path']}}"></div>
                                        @endif
                                    @endforeach
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-save-cover">Guardar</button>
            </div>
        </div>
    </div>
</div>
<script>

    var ed = false;
    try{
        if(editor){
            ed = true;
        }

    }catch(e){

    }
    <?php if(!$book_active):?>
        jQuery("#myModal").modal({
            backdrop: 'static',
            keyboard: false
        });
        jQuery("#myModal").modal('show');
    <?php endif; ?>
</script>
