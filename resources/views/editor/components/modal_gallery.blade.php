<!-- Modal -->
<div class="modal fade" id="galleryModal" tabindex="-1" role="dialog" aria-labelledby="galleryModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="galleryModalLabel">Subir imagen</h4>
            </div>
            <div class="modal-body">
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body panel-body-main-vertical">
                        <div class="styles-preview-select">
                            <img src="{{ asset('site_themes/'.Config::get('siteconfig.theme').'/images/theme-example.png') }}" />
                            <label>Otoño</label>
                        </div>
                        <div class="styles-preview-select">
                            <img src="{{ asset('site_themes/'.Config::get('siteconfig.theme').'/images/theme-example.png') }}" />
                            <label>Invierno</label>
                        </div>
                        <div class="styles-preview-select">
                            <img src="{{ asset('site_themes/'.Config::get('siteconfig.theme').'/images/theme-example.png') }}" />
                            <label>Fuego</label>
                        </div>
                        <div class="styles-preview-select">
                            <img src="{{ asset('site_themes/'.Config::get('siteconfig.theme').'/images/theme-example.png') }}" />
                            <label>Otro</label>
                        </div>
                        <div class="styles-preview-select">
                            <img src="{{ asset('site_themes/'.Config::get('siteconfig.theme').'/images/theme-example.png') }}" />
                            <label> y Otro</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>
<style>

    .modal-dialog .panel-body-main-vertical img {
        width: 100%;
    }

    .modal-dialog .panel-body-main-vertical label{
    }
    .modal-dialog .panel-collapse .panel-body {
        overflow-y: scroll;
    }
    .styles-preview-select {
        float: left;
        width: 49%;
        margin-right: 1%;
    }
</style>