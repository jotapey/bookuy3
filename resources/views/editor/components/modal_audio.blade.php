<!-- Modal -->
<div class="modal fade" id="modalAudio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Agregar Audio</h4>
            </div>
            <div class="modal-body">
                    <div class="">
                        <form>
                            <fieldset>
                                <div class="form-group">
                                    <label for="disabledTextInput">Origen</label>
                                    <div class="form-group">
                                        <label class="radio-inline"><input type="radio" name="audio_origin"  value="record" checked>Grabacion</label>
                                        <label class="radio-inline"><input type="radio" name="audio_origin"  value="upload">Subir archivo</label>
                                    </div>
                                </div>
                                <div class="form-group record-file">
                                    <p>
                                        <button class="btn btn-success" id="mstartRecord">Grabar</button>
                                        <button class="btn btn-danger" id="stopRecord">Detener</button>
                                    </p>
                                    <p>
                                        <audio id="recordedAudio"></audio>
                                    </p>
                                </div>
                                <div class="form-group upload-file hidden">
                                    <form id="audioupload" action="#" method="POST" enctype="multipart/form-data">
                                        <input type="file" class="form-control">
                                    </form>
                                </div>

                            </fieldset>
                        </form>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar
                </button>
                <button type="button" class="btn btn-primary btn-save-audio">Guardar</button>
            </div>
        </div>
    </div>
</div>