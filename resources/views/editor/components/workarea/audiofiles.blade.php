<div class="col-sm-11">
    <div class="panel panel-default work-panel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">
                    {{ trans('editor/workarea.work_area') }}
                </a>
            </li>
            <li role="presentation" class="">
                <a href="#profile" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="false">
                    {{ trans('editor/workarea.audio') }}
                </a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">
                <form class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <textarea class="form-control"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div role="tabpanel" class="tab-pane" id="profile">

            </div>
        </div>
    </div>
</div>