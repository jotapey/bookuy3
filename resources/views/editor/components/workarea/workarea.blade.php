<div id="loaded-css-links">

</div>
<div class="col-sm-12">
    <div class="panel panel-default work-panel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">
                    {{ trans('editor/workarea.work_area') }}
                </a>
            </li>
            <li role="presentation" class="">
                <a href="#profile" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="false">
                    {{ trans('editor/workarea.audio') }}
                </a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane tab-pane-workarea active" id="home">
                @include("editor/components/workarea/droparea")
            </div>

            <div role="tabpanel" class="tab-pane" id="profile">
                <button class="btn btn-primary play-all">Play</button>
                <div id="audios">
                    <table class="table audios-table">
                        <caption>Optional table caption.</caption>
                        <thead>

                        <tr>
                            <th>Audio</th>
                            <th>Esperar (seg)</th>
                            <th>No repetir</th>
                            <th>Repetir (esta pagina)</th>
                            <th>Repetir ( Hasta terminar el libro )</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <button class="btn btn-success" id=add_audio>Añadir Audio</button>
            </div>
            <hr>
            <button class="btn btn-default btn-guardar-item" <?php echo(isset($book_active) && $book_active)?"":"disabled" ?> >Guardar</button>
            <button class="btn btn-default btn-add-item" <?php echo(isset($book_active) && $book_active)?"":"disabled" ?> >Agregar nueva pagina</button>
            <a class="btn btn-primary btn-home" href="{{ url('home') }}">Volver</a>
        </div>
    </div>
</div>
