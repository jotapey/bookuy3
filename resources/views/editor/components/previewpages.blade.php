<div class="col-sm-12">
    <div class="panel panel-default pages-preview-panel">
        <div class="panel-heading">
            Paginas
            <div style="
                float: right;
                height: 24px;
            ">
                <button class="btn btn-primary btn-left-page" style="
                    height: 10px;
                    margin-top: 0;
                    line-height: 1px;
                    padding: 9px;
                ">Left</button>
               <button class="btn btn-primary btn-right-page" style="
                    height: 10px;
                    margin-top: 0;
                    line-height: 1px;
                    padding: 9px;
                ">Right</button>
            </div>
        </div>
        <div class="panel-body panel-body-main panel-body-horizontal owl-carousel">
            <div id="preview_cover"><img style="width: 200px; height: 100px;" src="{{ asset('site_themes/'.Config::get('siteconfig.theme').'/images/page-preview-example.png') }}" /><label>Tapa</label></div>
        </div>
    </div>
</div>