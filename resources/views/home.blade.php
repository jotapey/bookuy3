@extends('layouts.app')

@section('content')
    <script>
        window.Laravel = {"csrfToken":"TcFfW1ijIwI47uw3MhFzAVZBmOlStwJVjpit0ptg"}

        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })

    </script>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Mis Libros</div>
                    <div class="panel-body">
                        @foreach($books as $book)
                            @if(isset($book["items"][0]))
								@if ($book["items"][0]->type == "cover")
                                <div class="books-created">
                                    
                                        <img src="{{$book["items"][0]->thumbnail }}">
                                        <div class="home-book-title">{{$book["book"]->title}}</div>
                                        <a href="editor/{{$book["book"]->id}}" class="btn btn-success">Editar</a>
                                        <a href="ver/{{$book["book"]->id}}" class="btn btn-primary">Ver</a>
                                        <a href="#" book_id="{{$book["items"][0]->book_id}}" class="btn btn-danger">Eliminar</a>

										<?php 
											$pdf = "";
											if(is_file(base_path().'/public/books/'.$book["book"]->author_id.'/'.$book["book"]->id.'.pdf' )){
												$pdf = '/books/'.$book["book"]->author_id.'/'.$book["book"]->id.'.pdf';
											}
											if($pdf){
												echo '<a href="'.$pdf.'" class="btn btn-info">Descargar Pdf</a>';
											}
										?>
                                        
                                        <?php
                                        $class = "";
										$href = "#";
										$target = "";
                                        if(is_file( base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'books' . DIRECTORY_SEPARATOR . $book["book"]->author_id . DIRECTORY_SEPARATOR . "apk" . DIRECTORY_SEPARATOR . $book["items"][0]->book_id . ".apk")){
                                            $text = "Descargar";
											$target = 'target="_blank"';
											$href = 'books' . DIRECTORY_SEPARATOR . $book["book"]->author_id . DIRECTORY_SEPARATOR . "apk" . DIRECTORY_SEPARATOR . $book["items"][0]->book_id . ".apk";
                                        }else{
                                            $text = "Generar";
                                            $class = "generate-apk";
											
                                        }
                                        ?>
                                        <a <?php echo $target ?> href="{{$href}}" book_id="{{$book["items"][0]->book_id}}" author="{{$book["book"]->author_id}}" class="{{$class}} btn btn-info">{{$text}} Apk</a>
                                </div>
                                @endif
                            @endif
                        @endforeach
                        <hr>
                        <a href="editor" class="btn btn-default" role="button" style="margin-top: 5px;">
                            {{ trans('home.create_book') }}
                        </a>
                        <hr>
                        @if( Session::get('userRole') == 'Admin')
                            <a class="btn btn-default" href="{{ url('/admin') }}">{{ trans('home.admin_panel_link') }}</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('pages/home/js/general_scripts.js') }}"></script>

@endsection
