<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Josh Admin Template</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{ asset('admin_assets/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}"></script>
    <script src="{{ asset('admin_assets/https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') }}"></script>
    <![endif]-->
    <!-- global css -->
    <link href="{{ asset('admin_assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="{{ asset('admin_assets/vendors/font-awesome-4.2.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin_assets/css/styles/black.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin_assets/css/panel.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('admin_assets/css/metisMenu.css') }}" rel="stylesheet" type="text/css"/>
    <!-- end of global css -->
    <!--page level css -->
    <link href="{{ asset('admin_assets/vendors/fullcalendar/css/fullcalendar.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin_assets/css/pages/calendar_custom.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" media="all" href="{{ asset('admin_assets/vendors/jvectormap/jquery-jvectormap.css') }}" />
    <link rel="stylesheet" href="{{ asset('admin_assets/vendors/animate/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin_assets/css/only_dashboard.css') }}" />
    <link rel="stylesheet" href="{{ asset('admin_assets/vendors/select2/select2.css') }}" />
    <!--end of page level css-->
</head>

<body class="skin-josh">
<header class="header">
    <a class="navbar-brand" href="{{ url('/') }}">
        {{ config('app.name', 'Laravel') }}
    </a>
    @section('navbar')
        @include('layouts.sections.admin.navbar')
    @show
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    @section('left_menu')
        @include('layouts.sections.admin.left_menu')
    @show
    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Main content -->
        <section class="content-header">
            <h1>{{ trans('admin.dashboard') }}</h1>
            <ol class="breadcrumb">
                <li class="active">
                    <a href="#">
                        <i class="livicon" data-name="home" data-size="16" data-color="#333" data-hovercolor="#333"></i>
                        Home
                    </a>
                </li>
            </ol>
        </section>
<!-- global js -->
<script src="{{ asset('admin_assets/js/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin_assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!--Vendors-->
<script src="{{ asset('admin_assets/vendors/select2/select2.js') }}" type="text/javascript"></script>
<!--livicons-->
<script src="{{ asset('admin_assets/vendors/livicons/minified/raphael-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin_assets/vendors/livicons/minified/livicons-1.4.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin_assets/js/josh.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin_assets/js/metisMenu.js') }}" type="text/javascript"> </script>
<script src="{{ asset('admin_assets/vendors/holder-master/holder.js') }}" type="text/javascript"></script>
<!-- end of global js -->
<!-- begining of page level js -->
<!--  todolist-->
<script src="{{ asset('admin_assets/js/todolist.js') }}"></script>
<!-- EASY PIE CHART JS -->
<script src="{{ asset('admin_assets/vendors/charts/easypiechart.min.js') }}"></script>
<script src="{{ asset('admin_assets/vendors/charts/jquery.easypiechart.min.js') }}"></script>
<!--for calendar-->
<script src="{{ asset('admin_assets/vendors/fullcalendar/fullcalendar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin_assets/vendors/fullcalendar/calendarcustom.min.js') }}" type="text/javascript"></script>
<!--   Realtime Server Load  -->
<script src="{{ asset('admin_assets/vendors/charts/jquery.flot.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin_assets/vendors/charts/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
<!--Sparkline Chart-->
<script src="{{ asset('admin_assets/vendors/charts/jquery.sparkline.js') }}"></script>
<!-- Back to Top-->

<!--   maps -->
<script src="{{ asset('admin_assets/vendors/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('admin_assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('admin_assets/vendors/jscharts/Chart.js') }}"></script>
<script src="{{ asset('admin_assets/js/dashboard.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var composeHeight = $('#calendar').height() +21 - $('.adds').height();
        $('.list_of_items').slimScroll({
            color: '#A9B6BC',
            height: composeHeight + 'px',
            size: '5px'
        });
    });
</script>
<script src="{{ asset('admin_assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
<script src="{{ asset('admin_assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
<script src="{{ asset('admin_assets/vendors/iCheck/icheck.js') }}"></script>
<script>
$(document).ready(function() {
    $('input[type="checkbox"].custom-checkbox, input[type="radio"].custom-radio').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue',
        increaseArea: '20%'
    });
    $('#reset').click(function() {
        $('input').iCheck('uncheck');
    });

});
</script>
        <section class="content">
            @yield('content')
        </section>
    </aside>
    <!-- right-side -->
</div>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
    <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
</a>
<!-- end of page level js -->

</body>
</html>