<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <div>
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <div class="responsive_nav"></div>
        </a>
    </div>
    <div class="navbar-right">
        <ul class="nav navbar-nav">

            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <div class="riot">
                        <div>
                            {{ Auth::user()->name }}
                            <span>
                                        <i class="caret"></i>
                                    </span>
                        </div>
                    </div>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <!-- Menu Body -->
                    <li>
                        <a href="#">
                            <i class="livicon" data-name="user" data-s="18"></i>
                            My Profile
                        </a>
                    </li>
                    <li role="presentation"></li>
                    <li>
                        <a href="#">
                            <i class="livicon" data-name="gears" data-s="18"></i>
                            Account Settings
                        </a>
                    </li>
                    <li role="separator" class="divider"></li>
                    <!-- Menu Footer-->
                    <li class="user-footer">

                        <div class="">
                            <!--<a href="login.html">
                                <i class="livicon" data-name="sign-out" data-s="18"></i>
                                Logout
                            </a>-->

                            <a href="{{ url('/logout') }}"
                               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                                <i class="livicon" data-name="sign-out" data-s="18"></i>
                                Salir
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>