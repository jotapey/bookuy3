<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--<meta name="viewport" content="width=320">-->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('site_themes/'.Config::get('siteconfig.theme').'/css/app.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('general_assets/css/styles.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('general_assets/css/jquery-confirm.min.css') }}">

    <!-- Scripts -->
    <script>
        window.Laravel = '<?php echo json_encode(['csrfToken' => csrf_token(),]); ?>';
    </script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

    <link href="{{ asset('general_assets/js/jquery-ui-1.12.1/jquery-ui.min.css') }}" rel="stylesheet">
    <script src="{{ asset('general_assets/js/jquery-ui-1.12.1/jquery-ui.min.js') }}"></script>


    <!-- Latest compiled and minified Bootstrap -->
    <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            $('.dropdown-toggle').dropdown()
        });
    </script>
    <!-- Scripts -->
    <script>
        //Global
        var baseUrl = "{{ asset('')}}";
    </script>
    <!--<script src='{{ asset('site_themes/'.Config::get('siteconfig.theme').'/js/app.js') }}'></script>-->

    <script type="text/javascript" src="{{ asset('general_assets/js/jquery-confirm.min.js') }}"></script>
</head>
<body>
<div id="app">
    @section('navbar')
        @include('layouts.sections.navbar')
    @show
    @yield('content')
</div>


</body>
</html>
