@extends('layouts.app')
@section('content')
    <link href="{{ asset('/general_assets/css/default.css') }}" rel="stylesheet">
    <link href="{{ asset('/general_assets/css/bookblock.css') }}" rel="stylesheet">
    <link href="{{ asset('/general_assets/css/demo1.css') }}" rel="stylesheet">
    <!--<script src="js/modernizr.custom.js"></script>-->
    <?php
    /*echo "<pre>";
    print_r($book_active);
    print_r($loaded_items);
    die();*/
    ?>
    <script>
        var pages = null;
        var current_page = null;
        var audios = null;
    </script>
    <?php if($book_active):?>
    <?php if(count($loaded_items)):?>
    <div class="container">

        <div class="main clearfix">
            <div class="bb-custom-wrapper">
                <h3>Titulo: <?php echo $book_active->title ?></h3>
                <h3>Autor: <?php echo $book_active->author_name ?></h3>
                <div id="bb-bookblock" class="bb-bookblock">
                    <?php foreach($loaded_items as $loaded_item):?>
                    <div class="bb-item">
                        <img src="<?php echo $loaded_item["thumbnail"] ?>" alt="image01"/>
                    </div>
                    <?php endforeach; ?>
                </div>
                <nav>
                    <a id="bb-nav-first" href="#" class="bb-custom-icon bb-custom-icon-first">First page</a>
                    <a id="bb-nav-prev" href="#" class="bb-custom-icon bb-custom-icon-arrow-left">Previous</a>
                    <a id="bb-nav-next" href="#" class="bb-custom-icon bb-custom-icon-arrow-right">Next</a>
                    <a id="bb-nav-last" href="#" class="bb-custom-icon bb-custom-icon-last">Last page</a>
                </nav>
            </div>
        </div>
    </div><!-- /container -->
    <script>
        var DIRECTORY_SEPARATOR = "<?php echo DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR ?>"
        pages = JSON.parse('<?php echo json_encode($items_id) ?>');
        current_page = JSON.parse('<?php echo json_encode($items_id[0]) ?>');
        audios = JSON.parse('<?php echo json_encode($audios) ?>');
        console.log(pages)
        console.log(current_page)
        console.log(audios)
    </script>
    <?php endif; ?>
    <?php endif; ?>
    <script type="text/javascript" src="{{ asset('general_assets/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('general_assets/js/jquerypp.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('general_assets/js/jquery.bookblock.js') }}"></script>
    <script type="text/javascript" src="{{ asset('general_assets/js/viewer.js') }}"></script>
    <script>

        var Page = (function() {

            var config = {
                    $bookBlock : $( '#bb-bookblock' ),
                    $navNext : $( '#bb-nav-next' ),
                    $navPrev : $( '#bb-nav-prev' ),
                    $navFirst : $( '#bb-nav-first' ),
                    $navLast : $( '#bb-nav-last' )
                },
                init = function() {
                    config.$bookBlock.bookblock( {
                        speed : 780,
                        shadowSides : 0.8,
                        shadowFlip : 0.7
                    } );
                    initEvents();
                },
                initEvents = function() {

                    var $slides = config.$bookBlock.children();

                    // add navigation events
                    config.$navNext.on( 'click touchstart', function() {
                        config.$bookBlock.bookblock( 'next' );
                        return false;
                    } );

                    config.$navPrev.on( 'click touchstart', function() {
                        config.$bookBlock.bookblock( 'prev' );
                        return false;
                    } );

                    config.$navFirst.on( 'click touchstart', function() {
                        config.$bookBlock.bookblock( 'first' );
                        return false;
                    } );

                    config.$navLast.on( 'click touchstart', function() {
                        config.$bookBlock.bookblock( 'last' );
                        return false;
                    } );

                    // add swipe events
                    $slides.on( {
                        'swipeleft' : function( event ) {
                            config.$bookBlock.bookblock( 'next' );
                            return false;
                        },
                        'swiperight' : function( event ) {
                            config.$bookBlock.bookblock( 'prev' );
                            return false;
                        }
                    } );

                    // add keyboard events
                    $( document ).keydown( function(e) {
                        var keyCode = e.keyCode || e.which,
                            arrow = {
                                left : 37,
                                up : 38,
                                right : 39,
                                down : 40
                            };

                        switch (keyCode) {
                            case arrow.left:
                                config.$bookBlock.bookblock( 'prev' );
                                break;
                            case arrow.right:
                                config.$bookBlock.bookblock( 'next' );
                                break;
                        }
                    } );
                };

            return { init : init };

        })();
    </script>
    <script>
        Page.init();
    </script>
    <div id="audios"></div>
    <style>
        html{
            min-width: 800px !important;
        }
        .main.clearfix{
            margin-left: 0px !important;
            padding-left: 0px !important;
        }
        .container, .container-fluid {
            padding-left: 5px;
        }

    </style>
@endsection