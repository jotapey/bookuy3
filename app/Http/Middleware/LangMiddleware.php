<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\FileLoader;

class LangMiddleware
{
    public function changeTheme(){
        config(['siteconfig.theme' => 'default']);
        $fp = fopen(base_path() .'/config/siteconfig.php' , 'w');
        fwrite($fp, '<?php return ' . var_export(config('siteconfig'), true) . ';');
        fclose($fp);
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //$this->changeTheme();
        if (empty(session('custom_lang'))) {
            $lang = explode("-", $_SERVER['HTTP_ACCEPT_LANGUAGE'])[0];
            session(['custom_lang' => $lang]);
        }

        if (!empty(session('custom_lang'))) {
            \App::setLocale(session('custom_lang'));
        }

        return $next($request);
    }
}
