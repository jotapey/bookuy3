<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;

class ThemeWizzardController extends Controller
{

    public function index()
    {
        return view('theme_wizzard');
    }
}
