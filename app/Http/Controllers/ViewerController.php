<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\Session;
use App\Models\Book;
use App\Models\Audio;
use App\Models\Item;
//use Auth;

class ViewerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        //$this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($book_id = false)
    {
        $allow = false;
        if(isset($_GET["origin"])){
            if($_GET["origin"] == "mobile"){
                $allow = true;
            }
        }

        if(!$allow){
            $user = Auth::user();

            if(!$user->email){
                return redirect('login');
            }
        }
        $current_book = false;
        $loaded_items = array();
        $book_active = false;

        if($book_id){
            $current_book = Book::getLibro($book_id);
            if(count($current_book)){
                $book_active = $current_book[0]["book"];
                $book_active->css = json_decode($book_active->css);
                foreach($current_book[0]["items"] as $item){
                    if(strpos($item->thumbnail, "[")!==false){
                        $item->thumbnail = json_decode($item->thumbnail);
                        $item->thumbnail = $item->thumbnail[0];
                    }
                    $loaded_items[] = array(
                        "id" => $item->id,
                        "book_id" => $book_id,
                        "thumbnail" => $item->thumbnail,
                        "type" => $item->type,
                    );
                }
            }
        }

        $audios = array();
        $items_id = array();
        foreach($loaded_items as $item){
            $tmps = Audio::getAll(array("item_id" => $item["id"]));
            foreach($tmps as $tmp){
                $audios[] = $tmp;
            }
        }

        foreach($loaded_items as $item){
            $tmp = $item["id"];
            $items_id[] = $tmp;
        }

        $loaded_items = (array)$loaded_items;

        return view('viewer', [
            
            'paginas' => $this->getCovers(),
            'loaded_items' => $loaded_items,
            'items' => $this->getCovers("pages"),
            'book_active' => $book_active,
            'items_id' => $items_id,
            'audios' => $audios
        ]);
    }

    public function getCovers($get="covers"){
        $covers = array();

        $files = File::allFiles(public_path().DIRECTORY_SEPARATOR."book_themes".DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR.$get);


        $covers_folders = array();
        $covers = array();

        foreach ($files as $file){
            $folder = (string)$file->getRelativePath();
            if(!in_array($folder, $covers_folders)){
                $covers_folders[] = $folder;
            }
        }

        foreach($covers_folders as $cover){
            $file = File::allFiles(public_path().DIRECTORY_SEPARATOR."book_themes".DIRECTORY_SEPARATOR.$get.DIRECTORY_SEPARATOR.$cover);
            $resources = array();
            foreach ($file as $item){
                $type="img";
                if(strpos($item->getRelativePathname(), ".php") !== false){
                    $type="template";
                }

                if(strpos($item->getRelativePathname(), ".css") !== false){
                    $type="css";
                }

                $path = $item->getRelativePathname();
                $name = $cover;

                $resource = array(
                    "type" => $type,
                    "path" => $path,
                    "name" => $name
                );

                $resources[] = $resource;
            }

            $covers[] = $resources;

        }

        return $covers;
    }
}
