<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace App\Http\Controllers;
require __DIR__.'/../../../vendor/autoload.php';
use PDF;

use GDText\Box;
use GDText\Color;
use App\ItemFactory;
use App\Page;
use App\Models\Item;
use App\Models\Audio;
use League\Flysystem\Exception;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use File;
use App\Models\Book;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Description of AjaxController
 *
 * @author jotapey
 */
class AjaxController {

    public function __construct(){
        $this->editorController = new EditorController();
    }

    public function uploadPreAudio(){
        $ret = array();

        if(isset($_FILES["audio_file"])){
            $ret = $this -> uploadAudio($_FILES, $_POST["loop"], $_POST["delay"],  $_POST["item_id"], $_POST["id"]);
        }
        if($ret && count($ret)){
            return Response::json(array('status' => 'success','url_audio' => $ret[0]));
        }

        return Response::json(array('status' => 'false'));
    }

    public function setTitleStyleBook(){
        $this->editorController;
    }

    public function index(){

        if(Request::ajax()){
            $id = Input::get('id');
            $type = Input::get('type');
            return Response::json(array('html' => $this->getHtmlCover($id,$type)));
        }
        return response()->view('errors.request', [], 500);
    }

    private function getHtmlItem($type,$id){
        $items = false;

        $files = File::allFiles(public_path().DIRECTORY_SEPARATOR."book_themes".DIRECTORY_SEPARATOR.$type.DIRECTORY_SEPARATOR.$id);

        foreach($files as $file){
            if(strpos($file->getRelativePathname(), ".php") !== false){
                $html = file_get_contents(public_path().DIRECTORY_SEPARATOR."book_themes".DIRECTORY_SEPARATOR.$type.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR.$file->getRelativePathname());
                $items = $html;
            }
        }

        return $items;
    }

    private function getHtmlCover($id,$type){
        return $this->getHtmlItem($type,$id);
    }

    public function saveOrUpdateItem(){
        $action = "update";
        $id = Input::get('id');
        if(Request::ajax()){
            if(isset($_POST["id"])){
                $item = new Item();

                foreach($_POST as $key => $value){
                    $item->$key =$value;
                }

                $id = Input::get('id');


                $item->images = null;

                if(isset($_POST["image-0"])){

                    $img = $_POST['image-0'];
                    $img = str_replace('data:image/jpeg;base64,', '', $img);
                    $img = str_replace(' ', '+', $img);
                    $data = base64_decode($img);

                    $url = url('books' . DIRECTORY_SEPARATOR . $_POST["author_id"]);
                    $dir_subida = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'books' . DIRECTORY_SEPARATOR . $_POST["author_id"] . DIRECTORY_SEPARATOR;
                    if(!is_dir($dir_subida)){
                        mkdir($dir_subida);
                    }

                    $file = md5(date("php date y m d hh mm ss")).".jpg";
                    $url_tmp = $url . DIRECTORY_SEPARATOR . $file;
                    $fichero_subido = $dir_subida . $file;
                    /*
                                        if (move_uploaded_file($file['tmp_name'], $fichero_subido)) {
                                            $images_return[] = $url_tmp;
                                        }*/

                    $success = file_put_contents($fichero_subido, $data);
                }

                if($success){
                    //if(count($_FILES)){
                    //$images = $this->uploadImages($_FILES,$_POST["author_id"],$_POST["book_id"]);
                    //if(count($images)){
                    //$item->images = json_encode($images);
                    $item->images = ($file);
                    //}
                }

                $item->status = "active";

                $inserted = Item::saveOrUpdate((object) $item);

                if(!$id || $id == "false"){
                    $action = "save";
                    $item->id = $inserted;
                }



                return Response::json(array('action' => $action, 'status' => 'success','active_item' => $item));
            }
        }

        return Response::json(array('action' => $action, 'status' => 'error'));
    }

    public function updateItemsPos(){
        $error = 'success';
        if(isset($_POST["items"])){
            try{
                $items = $_POST["items"];

                foreach($items as $item){
                    $updated = Item::updateOrder($item["item_id"], $item["pos"]);
                    if(!$updated){
                        $error = 'error';
                    }
                }
                return Response::json(array('status' => 'success'));
            } catch (Exception $e) {
                return Response::json(array('status' => 'error','message' =>$e->getMessage()));
            }
        }
        return Response::json(array('status' => 'error','message' => 'no items selected'));
    }

    public function saveOrUpdateAudio(){

        $saved_audios = array();
        if(Request::ajax()){
            $book_id = $_POST["book_id"];
            $item_id = $_POST["item_id"];
            if(isset($_POST["send_data"])){
                $audios = $_POST["send_data"];

                foreach($audios as $audio){
                    $tmpAudio = new Audio();
                    $tmpAudio->item_id = $item_id;
                    foreach($audio as $key => $value){
                        $tmpAudio->$key =$value;
                    }

                    $a = Audio::saveOrUpdate($tmpAudio,$audio["id"]);

                    if($a){
                        $saved_audios[] = $a;
                    }
                }
            }

        }
        if($saved_audios && count($saved_audios)){
            return Response::json(array('status' => 'succes', 'saved_audios' => $saved_audios));
        }

        return Response::json(array('status' => 'false'));
    }

    public function deleteItem(){
        if(isset($_POST["item_id"]) && $_POST["item_id"]){
            $delete = Item::deleteItem($_POST["item_id"]);
            if($delete){
                return Response::json(array('status' => 'success'));
            }
            return Response::json(array('status' => 'error'));
        }

        return Response::json(array('status' => 'error'));
    }

    private function uploadImages($files,$author_id,$book_id){
        $images_return = array();
        $url = url('books' . DIRECTORY_SEPARATOR . $author_id);
        $dir_subida = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'books' . DIRECTORY_SEPARATOR . $author_id.DIRECTORY_SEPARATOR;
        if(!is_dir($dir_subida)){
            mkdir($dir_subida);
        }

        foreach($files as $file){
            $md5 = md5(date("php date y m d hh mm ss")).$file['name'];
            $url_tmp = $url . DIRECTORY_SEPARATOR . $md5;
            $fichero_subido = $dir_subida . $md5;

            if (move_uploaded_file($file['tmp_name'], $fichero_subido)) {
                $images_return[] = $url_tmp;
            }
        }

        return $images_return;
    }
//($_FILES, $_POST["item_id"], $_POST["loop"], $_POST["delay"])
    private function uploadAudio($files, $loop, $delay,$item_id = null,$id = null){
        $images_return = array();
        $url = url('books' . DIRECTORY_SEPARATOR  . 'audios');
        $dir_subida = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'books' . DIRECTORY_SEPARATOR . "audios" . DIRECTORY_SEPARATOR;
        if(!is_dir($dir_subida)){
            mkdir($dir_subida);
        }

        foreach($files as $file){
            if($id == "null"){
                $id = null;
            }
            if($item_id == "null"){
                $item_id = null;
            }

            $ui = $id;

            $data = array(
                "id" => $ui,
                "item_id" => $item_id,
                "loop" => $loop,
                "delay" => $delay
            );

            if(!$id){

                $ui = uniqid();
                $file['name'] = $ui.".mp3";
                $name = $file['name'];
                $url_tmp = $url . DIRECTORY_SEPARATOR . $name;
                $fichero_subido = $dir_subida . $name;

                if (!move_uploaded_file($file['tmp_name'], $fichero_subido)) {
                    return $images_return;
                }

                $images_return[] = array(
                    "url" => $url_tmp,
                    "id" => $ui
                );
                $data = (object)$data;
                $data->audio = $ui;
            }

            if(Audio::saveOrUpdate($data)){
                return $images_return;
            }
        }

        return false;
    }

	public function generateCredits($book_id, $title, $author, $author_id){
		
		
		$im = imagecreatetruecolor(800, 400);
		$backgroundColor = imagecolorallocate($im, 0, 18, 64);
		imagefill($im, 0, 0, $backgroundColor);

		$box = new Box($im);

		$box = new Box($im);
		$box->setFontFace(base_path().'/public/LinLibertine_R.ttf'); // http://www.dafont.com/franchise.font
		$box->setFontSize(40);
		$box->setFontColor(new Color(255, 255, 255));
		$box->setTextShadow(new Color(0, 0, 0, 50), 0, -2);
		$box->setBox(20, 20, 600, 600);
		$box->setTextAlign('left');
		$box->draw("Titulo: ".$title);

		$box = new Box($im);
		$box->setFontFace(base_path().'/public/LinLibertine_R.ttf'); // http://www.dafont.com/franchise.font
		$box->setFontSize(40);
		$box->setFontColor(new Color(255, 255, 255));
		$box->setTextShadow(new Color(0, 0, 0, 50), 0, -2);
		$box->setBox(20, 60, 600, 600);
		$box->setTextAlign('left');
		$box->draw("Autor: ".$author);		

		
		$dir_subida = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'books' . DIRECTORY_SEPARATOR . $author_id . DIRECTORY_SEPARATOR;
		if(!is_dir($dir_subida)){
			mkdir($dir_subida);
		}

		$file = md5(date("php date y m d hh mm ss")).".jpg";
		$fichero_subido = $dir_subida . $file;

		

		header("Content-type: image/png");
		imagepng($im, $fichero_subido);
		
		$item = new Item();
		
		$item->id = null;
		$item->type = "credits";
		$item->texts = "";
		$images = array(url('/')."//"."books"."//".$author_id."//".$file);
		$item->images = json_encode($images);
		$item->book_id = $book_id;
		$item->status = "active";
		$item->order = 0;
		$item->thumbnail = "";
		$item->html = "";
		
		$inserted = Item::saveOrUpdate((object) $item);
		
	}

	/*public function generateCredits($book_id, $title, $author, $author_id){
		
		$im = imagecreatetruecolor(800, 400);
		$color_texto = imagecolorallocate($im, 0, 0, 0);
		$white = imagecolorallocate($im, 255, 255, 255);
		imagefill($im, 0, 0, $white);
		imagestring($im, 5, 100, 100,  "Titulo: ".$title, $color_texto);
		imagestring($im, 5, 100, 200,  "Autor: ".$author, $color_texto);

		// Guardar la imagen como 'textosimple.jpg'		
		$dir_subida = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'books' . DIRECTORY_SEPARATOR . $author_id . DIRECTORY_SEPARATOR;
		if(!is_dir($dir_subida)){
			mkdir($dir_subida);
		}

		$file = md5(date("php date y m d hh mm ss")).".jpg";
		$fichero_subido = $dir_subida . $file;
		imagejpeg($im, $fichero_subido);
		
		$item = new Item();
		
		$item->id = null;
		$item->type = "credits";
		$item->texts = "";
		$images = array(url('/')."//"."books"."//".$author_id."//".$file);
		$item->images = json_encode($images);
		$item->book_id = $book_id;
		$item->status = "active";
		$item->order = 0;
		$item->thumbnail = "";
		$item->html = "";
		
		$inserted = Item::saveOrUpdate((object) $item);

		// Liberar memoria
		imagedestroy($im);
	}*/
	
    public function saveOrUpdateBook(){
        $book = new Book();
        $action = "update";
        $last_inserted = false;
        if(Request::ajax()){
            if(isset($_POST["item_data"])){

                $active_book = $_POST["item_data"];
                $id = Input::get('id');

                if(!$active_book["id"]){
                    $active_book["css"] = json_encode($active_book["css"]);
                    $active_book["author_id"] = Auth::user()->id;
                    $action = "save";
					
                    $last_inserted = Book::saveOrUpdate((object) $active_book);
					$tmp_data = ((object) $active_book);
					$book_title = $tmp_data->title;
					$book_author_name = $tmp_data->author_name;
					$this -> generateCredits($last_inserted, $book_title, $book_author_name, $active_book["author_id"]);
                }

                if($last_inserted !== false){
                    $active_book["id"] = $last_inserted;
                    $this -> generateApk($active_book["author_id"], $active_book["id"]);
                    return Response::json(array('action' => $action, 'status' => 'success','book_active' => $active_book));
                }
            }
        }
        return Response::json(array('action' => $action, 'status' => 'error'));
    }

    public function loadHtmlItemSaved(){
        if(isset($_POST["selected_item"]) && $_POST["selected_item"]){
            $selected_item = $_POST["selected_item"];

            $html = "";
            $tmpaudios = array();

            $item = Item::getAll([
                ['book_id', '=', $selected_item["book_id"]],
                ['id', '=', $selected_item["item_id"]],
            ]);

            if(count($item)){
                $item = $item[0];
                $html = $item->html;
                $audios = Audio::getAll([
                    ['item_id', '=', $item->id],
                ]);

                if(isset($audios) && $audios){
                    foreach($audios as $audio){
                        $audio->audio =  ".." . DIRECTORY_SEPARATOR . "books" . DIRECTORY_SEPARATOR . "audios" . DIRECTORY_SEPARATOR . $audio->audio.".mp3";
                        $tmpaudios[] = $audio;
                    }
                }

                return Response::json(array('status' => 'success', 'html' => $html,'audios' => $tmpaudios));
            }


        }

        return Response::json(array('status' => 'error'));
    }

    public function saveThumbnail(){
        $data = $_POST['data'];
        $book_id = $_POST['book_id'];
        $item_id = $_POST['item_id'];
        $file = md5(uniqid()) . '.png';


        $uri =  substr($data,strpos($data,",")+ 1);
        $url = url('books' . DIRECTORY_SEPARATOR . $book_id . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;

        $path = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'books' . DIRECTORY_SEPARATOR . $book_id . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR;

        if(is_dir($path)){
            file_put_contents($path.$file, base64_decode($uri));
        }else{

            mkdir($path);

            file_put_contents($path.$file, base64_decode($uri));
        }

        $updateItem = DB::table('items')->where([
            ['id', '=', $item_id],
        ])->get();

        $updateItem = $updateItem[0];

        $updateItem->thumbnail = $url.$file;
        $updateItem = (array)$updateItem;
        $update = DB::table('items')->where([
            ['id', '=', $item_id],
        ])->update($updateItem);


        echo json_encode(array("file" => $file,"path" => $path));
    }

    public function deleteBook(){
        if(isset($_POST["book_id"]) && $_POST["book_id"]){
            $book_id = $_POST["book_id"];

            $delete = Book::delete_book($book_id);
            if($delete){
                die( json_encode(array("success" => true)));
            }

        }

        echo json_encode(array("success" => false));
    }

    /* APK */

    public function copyAppFiles($autor_id,$book_id){

        $src = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . "bookuy_app" . DIRECTORY_SEPARATOR . "bookuy";
        $dst = $dir_subida = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'books' . DIRECTORY_SEPARATOR . $autor_id . DIRECTORY_SEPARATOR . "app_source";
        if(!is_dir($dir_subida)){
            mkdir($dir_subida);
        }

        $command = 'cp -r ' . $src . ' ' .$dst;
        $shell_result_output = shell_exec(($command));
        $this -> createIndex($autor_id, $book_id);
        $this -> zipFiles($autor_id, $book_id);
    }

    public function createIndex($autor_id, $book_id){
        $appUrl = \Config::get('app.url');
        $fileLocation = $dir_subida = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'books' . DIRECTORY_SEPARATOR . $autor_id . DIRECTORY_SEPARATOR . "app_source" . DIRECTORY_SEPARATOR . "bookuy" . DIRECTORY_SEPARATOR . "www" . DIRECTORY_SEPARATOR . "index.html";

        /*$command = "rm -rf $fileLocation";
        $shell_result_output = shell_exec(($command));*/

        $file = fopen($fileLocation,"w");

        $content = '<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="msapplication-tap-highlight" content="no" />
        <!-- WARNING: for iOS 7, remove the width=device-width and height=device-height attributes. See https://issues.apache.org/jira/browse/CB-4323 -->
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
        <link rel="stylesheet" type="text/css" href="css/index.css" />
        <title>Bookuy</title>
    </head>
    <body>
        <div class="app">
            
        </div>
        <script type="text/javascript" src="cordova.js"></script>
        <script type="text/javascript" src="js/index.js"></script>
        <script type="text/javascript">
            app.initialize();
			window.open("'. $appUrl .'/ver/'.$book_id.'?origin=mobile","_self");
        </script>
    </body>
</html>
';
        fwrite($file,$content);
        fclose($file);
    }

    public function generateApk($autor_id, $book_id){
        $this -> copyAppFiles($autor_id, $book_id);
    }

    public function zipFiles($autor_id, $book_id){
        $source = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'books' . DIRECTORY_SEPARATOR . $autor_id . DIRECTORY_SEPARATOR . "app_source" . DIRECTORY_SEPARATOR . "bookuy" ;
        $dst = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'books' . DIRECTORY_SEPARATOR . $autor_id . DIRECTORY_SEPARATOR . "app_source" . DIRECTORY_SEPARATOR ;
        $command = "zip -r ".$dst."book_$book_id.zip $source";
        $shell_result_output = shell_exec(($command));

        $this -> phonegapBuildExec($autor_id,$book_id);
    }

    public function phonegapBuildExec($autor_id,$book_id){

        $dst = base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'books' . DIRECTORY_SEPARATOR . $autor_id . DIRECTORY_SEPARATOR . "app_source" . DIRECTORY_SEPARATOR ."book_$book_id.zip";

        $command = "curl -u juanbarla@gmail.com:q1w2e3r4T% -X PUT -F file=@".$dst." https://build.phonegap.com/api/v1/apps/2712485";

        $shell_result_output = shell_exec(($command));

        $command = "curl -u juanbarla@gmail.com:q1w2e3r4T% -X POST -d '' https://build.phonegap.com/api/v1/apps/2712485/build";

        $shell_result_output = exec(($command));
    }

    public function makeApk(){
        if(isset($_POST["author"],$_POST["book_id"])){
            $autor_id = $_POST["author"];
            $book_id = $_POST["book_id"];

            if(!is_dir( base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'books' . DIRECTORY_SEPARATOR . $autor_id . DIRECTORY_SEPARATOR . "apk" )){
                mkdir(base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'books' . DIRECTORY_SEPARATOR . $autor_id . DIRECTORY_SEPARATOR . "apk");

            }

            $command = "curl -Lu juanbarla@gmail.com:q1w2e3r4T% https://build.phonegap.com/api/v1/apps/2712485/android > " .base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'books' . DIRECTORY_SEPARATOR . $autor_id . DIRECTORY_SEPARATOR . "apk" . DIRECTORY_SEPARATOR . $book_id.".apk" ;

            $shell_result_output = exec(($command));
            die("success");
        }

        die("error");
    }
}
