<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Auth;
use App\Models\Book;

use PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rol =  Session::get('userRole');

        $user = Auth::user();

        if(!$user->email){
            return redirect('login');
        }

        $books = Book::getLibros( $user->id);

		

		foreach($books as &$book){
			if(count($book["items"])){
				if(!is_file(base_path().'/public/books/'.$user->id.'/'.$book["book"]->id.'.pdf' )){
					$pdf_path = $this->generatePdf($book["book"]->id, $book,$user);
					$book["pdf_path"] = $pdf_path;
				}
			}
		}

		//echo "<pre>";print_r($books);die();

        return view('home', ['books' => $books]);
    }

	public function generatePdf($book_id, $book,$user){
		$return = "";
		
		$images = array();

		
		foreach($book["items"] as $item){
			if(is_file(base_path().'/public/books/'.$user->id.'/'.$item->images)){
				$images[] = base_path().'/public/books/'.$user->id.'/'.$item->images;
			}	
		}		

		/*if($book_id == "638"){
			echo "<pre>";print_r($book["items"]);die();
		}*/

		if(count($images)){
			/*echo "<pre>";
			print_r(array(
				"path" => base_path().'/public/books/'.$user->id.'/'.$book["book"]->id.'.pdf',
				"images" => $images
			));die();*/
			PDF::SetAutoPageBreak(false, 0);
			foreach($images as $image){
				PDF::AddPage();
				PDF::Image($image, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
				PDF::setPageMark();
			}
			PDF::Output(base_path().'/public/books/'.$user->id.'/'.$book_id.'.pdf', 'F');
			$return = base_path().'/public/books/'.$user->id.'/'.$book_id.'.pdf';
		}
		
		return $return;
		
	}
}
