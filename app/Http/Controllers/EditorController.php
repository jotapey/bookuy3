<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Support\Facades\Session;
use App\Models\Book;
//use Auth;

class EditorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($book_id = false)
    {
        
        $user = Auth::user();

        if(!$user->email){
            return redirect('login');
        }

        $current_book = false;
        $loaded_items = array();
        $book_active = false;

        if($book_id){
            $current_book = Book::getLibro($book_id);
            if(count($current_book)){
                $book_active = $current_book[0]["book"];
                $book_active->css = json_decode($book_active->css);
                foreach($current_book[0]["items"] as $item){
                    if(strpos($item->thumbnail, "[")!==false){
                        $item->thumbnail = json_decode($item->thumbnail);
                        $item->thumbnail = $item->thumbnail[0];
                    }
                    $loaded_items[] = array(
                        "id" => $item->id,
                        "book_id" => $book_id,
                        "thumbnail" => $item->thumbnail,
                        "type" => $item->type,
                    );
                }
            }
        }

        $loaded_items = (array)$loaded_items;

        return view('editor', [
            'user_id' => $user->id,
            'paginas' => $this->getCovers(),
            'loaded_items' => $loaded_items,
            'items' => $this->getCovers("pages"),
            'book_active' => $book_active
        ]);
    }

    public function getCoverAjax(){
        die("a");
    }
    
    public function getCovers($get="covers"){
        $covers = array();
        
        $files = File::allFiles(public_path().DIRECTORY_SEPARATOR."\book_themes".DIRECTORY_SEPARATOR.$get);
        
        
        $covers_folders = array();
        $covers = array();
        
        foreach ($files as $file){
            $folder = (string)$file->getRelativePath();
            if(!in_array($folder, $covers_folders)){
                $covers_folders[] = $folder;
            }
        }
        
        foreach($covers_folders as $cover){
            $file = File::allFiles(public_path().DIRECTORY_SEPARATOR."\book_themes".DIRECTORY_SEPARATOR.$get.DIRECTORY_SEPARATOR.$cover);
            $resources = array();
            foreach ($file as $item){
                $type="img";
                if(strpos($item->getRelativePathname(), ".php") !== false){
                    $type="template";
                }

                if(strpos($item->getRelativePathname(), ".css") !== false){
                    $type="css";
                }

                $path = $item->getRelativePathname();
                $name = $cover;

                $resource = array(
                    "type" => $type,
                    "path" => $path,
                    "name" => $name
                ); 
            
                $resources[] = $resource;
            }
            
            $covers[] = $resources;
            
        }
        
        return $covers;
    }

    /*private function getItems(){
        $items = array();

        $files = File::allFiles(resource_path().DIRECTORY_SEPARATOR."\book_themes".DIRECTORY_SEPARATOR."pages");


        $items_folders = array();
        $items = array();

        foreach ($files as $file){
            $folder = (string)$file->getRelativePath();
            if(!in_array($folder, $items_folders)){
                $items_folders[] = $folder;
            }
        }

        foreach($items_folders as $item){
            $file = File::allFiles(resource_path().DIRECTORY_SEPARATOR."\book_themes".DIRECTORY_SEPARATOR."pages".DIRECTORY_SEPARATOR.$item);
            $resources = array();
            foreach ($file as $i){
                $type="img";
                if(strpos($i->getRelativePathname(), ".php") !== false){
                    $type="template";
                }

                $path = $i->getRelativePathname();
                $name = $item;

                $resource = array(
                    "type" => $type,
                    "path" => $path,
                    "name" => $name
                );

                $resources[] = $resource;
            }

            $items[] = $resources;

        }

        return $items;
    }*/
    
    /*public function getThemes(){
        $themes = array();
        /*$theme = new \stdClass();
        $theme->name = "fuego";
        
        $otroTheme = new \stdClass();
        $otroTheme->name = "otro";
        
        $themes[] = $theme;
        $themes[] = $otroTheme;*//*
        $files = File::allFiles(public_path().DIRECTORY_SEPARATOR."book_themes".DIRECTORY_SEPARATOR."css");
        foreach ($files as $file)
        {
            $filePath = $file;
            $publicPos = strpos($filePath,"public");
            if($publicPos !== false){
                $filePath = substr($filePath,$publicPos+7,strlen($filePath));
            }
            $path = explode(DIRECTORY_SEPARATOR,$file);
            $file = $path[count($path)-1];
            if($file && strpos($file,".css")){
                $file = ucwords(substr($file, 0,strlen($file)-4));
                $theme = new \stdClass();
                $theme->name = $file;
                $theme->path = $filePath;
                $themes[] = $theme;
            }
        }
        return $themes;
    }*/
    
    /**/
    public function step1_load(){
        return redirect('home');
    }

    public function step2_load(Request $request){
        return redirect('home');
    }

    public function step3_load(Request $request){
        return redirect('home');
    }

    public function step4_load(Request $request){
        return redirect('home');
    }

    public function step1(Request $request){
        $files = $this->loadCoverTemplates();
        $bookId  = $request->input('book_id', false);
        $action  = $request->input('action', false);

        $book = \App\Book::find($bookId);

        if($bookId){
            if($action == "delete"){
                if($book){

                    foreach ($book->Pages as $page){
                        $page->delete();
                    }

                    $book->delete();

                    $request->session()->flash('alert-success', 'Libro eliminado!');
                    return redirect('home');
                }
            }
        }

        return view('step1', ['files' => $files, "book" => $book]);
    }

    public function uploadImg(){
        $user = Auth::user();
        $userPath = base_path()."/public/images/users/" . $user->id."/";
        $result = array(
            "error" => true,
            "message" => "Ocurrio un error al subir la imagen"
        );

        $imgConfig = array(
            "sizeLimit" => 500000,
            "allowedFileFormats" => array("jpg", "png", "gif", "jpeg")
        );

        if($this->findUserDir($user,$userPath) == false){
            $this->makeUserDir($userPath);
        }

        $target_dir = $userPath;
        $target_file = $target_dir . basename($_FILES["img_cover"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        $check = getimagesize($_FILES["img_cover"]["tmp_name"]);
        if($check !== false) {
            // Check if file already exists
            if (file_exists($target_file)) {
                $result = array (
                    "error" => false,
                    "file_url" => $target_file,
                    "image_name" => $_FILES["img_cover"]["name"]
                );
            }

            // Check file size
            if ($_FILES["img_cover"]["size"] > $imgConfig["sizeLimit"]) {
                $result = array (
                    "error" => true,
                    "file_url" => "",
                    "message" => "Sorry, your file is too large."
                );
            }

            // Allow certain file formats
            if(!in_array($imageFileType,$imgConfig["allowedFileFormats"]) ) {
                $result = array (
                    "error" => true,
                    "file_url" => "",
                    "message" => "Sorry, only JPG, JPEG, PNG & GIF files are allowed."
                );
            }

            // Check if $uploadOk is set to 0 by an error
            if (move_uploaded_file($_FILES["img_cover"]["tmp_name"], $target_file)) {
                $result = array (
                    "error" => false,
                    "file_url" => $target_file,
                    "image_name" => $_FILES["img_cover"]["name"]
                );
            }

        }

        if(!$result["error"]){
            $pos = strpos($result["file_url"], "public/");
            $result["file_url"] = substr($result["file_url"],$pos+strlen("public/"),strlen($result["file_url"]));
        }

        return $result;
    }

    public function findUserDir($user,$userPath){
        return is_dir($userPath);
    }

    public function makeUserDir($userPath){
        mkdir($userPath);
    }

    public function step2(Request $request){
        $bookImg = $request->input('image_path');
        $imageName = $request->input('image_name', false);
        if(isset($_FILES) && count($_FILES)){
            if(isset($_FILES["img_cover"]["name"]) && $_FILES["img_cover"]["name"]){
                $uploadImg = $this->uploadImg();
                if(!$uploadImg["error"]){
                    $bookImg = $uploadImg["file_url"];
                    $imageName = $uploadImg["image_name"];
                }
            }
        }

        $top = 100;

        $eventForm = $request->input('event_form');
        $bookTitle = $request->input('book-title');
        $bookAuthor = $request->input('book-author');
        $coverTemplate = $request->input('cover_template');
        $books = \App\Book::all();
        $bookId = 1;
        if(count($books)){
            //422
            $top += (count($books) * 489);
            $book = $books[count($books)-1];
            $bookId = $book->id + 1;
        }

        return view('step2', ["top" => $top, 'cover_template' => $coverTemplate,'title' => $bookTitle,'author' => $bookAuthor, "bookId" => $bookId, "bookImg" => $bookImg, "image_name" => $imageName ]);
    }

    public function step3(Request $request){

        $files = $this->loadPageTemplates();
        $event_form = $request->input('event_form',false);
        $coverTemplate = $request->input('cover_template');
        $bookCoverImg = $request->input('image_path');
        $bookTitle = $request->input('book-title',false);
        $bookAuthor = $request->input('book-author',false);
        $htmlCover = $request->input('html_cover',false);
        $htmlPage = $request->input('html_page',false);
        $eventForm = $request->input('event_form', false);
        $bookId  = $request->input('book_id', false);
        $pageNumber  = $request->input('page_number', false);
        $imageName = $request->input('image_name', false);
        $pageTemplate = $request->input('page_template', false);
        $res = false;


        $user = Auth::user();
        if($user){
            if($htmlCover){
                $res = $this->saveBookCover($user,$bookTitle,$bookAuthor,$htmlCover,$bookCoverImg,$coverTemplate,$imageName);
            }else{

                $res = $this->saveBookPage($eventForm,$bookId,$pageNumber,$htmlPage,$pageTemplate,$imageName);
            }
        }

        if(isset($res["book"])){
            if(!$bookId){
            $bookId = $res["book"]->id;
            }
        }

        if($event_form == "end"){
            return redirect('home');
        }

        return view('step3', ['files' => $files,"bookId" => $bookId]);
    }

    public function saveBookCover($user,$bookTitle,$bookAuthor,$htmlCover,$bookCoverImg,$coverTemplate,$image_name){
        $book = \App\Book::where("title" , '=', $bookTitle)->first();
        if(!$book){
            $book = new \App\Book();
            $book->id_author = $user->id;
            $book->title = $bookTitle;
            $book->author_name = $bookAuthor;
            $book->cover_html = $htmlCover;
            $book->image_path = $bookCoverImg;
            $book->cover_template = $coverTemplate;
            $book->image_name = $image_name;

            $book->status = "in_process";

            $book->save();

            return array(
                "status" => "saved",
                "book" => $book,
            );
        }else{
            if(
                $book->title != $bookTitle ||
                $book->author_name != $bookAuthor ||
                $book->cover_html != $htmlCover
            ){

                $book->title = $bookTitle;
                $book->author_name = $bookAuthor;
                $book->cover_html = $htmlCover;
                $book->image_path = $bookCoverImg;
                $book->cover_template = $coverTemplate;
                $book->image_name = $image_name;

                $book->update();

                return array(
                    "status" => "updated",
                    "book" => $book,
                );
            }
        }

        return false;
    }

    public function saveBookPage($event,$idBook,$pageNumber,$html,$pageTemplate,$imageName){

        if($event == "save" || $event == "end"){
            $page = new \App\Page();

            $page->book_id = $idBook;
            $page->page_number = $pageNumber;
            $page->html = $html;
            $page->page_template = $pageTemplate;
            $page->image_name = $imageName;

            $page->image_path = "imagePath";

            $page->save();
            return "saved";
        }else{
            $book = \App\Book::find($idBook);
            $idPage = null;
            $page = \App\Page::find($idPage);
            if(
                $page->page_number != $pageNumber ||
                $page->html != $idBook ||
                $page->image_path != "imagePath"
            ){
                $page->book_id = $idBook;
                $page->page_number = $pageNumber;
                $page->html = $html;
                $page->image_path = "imagePath";

                $page->update();
                return "updated";
            }
        }

        return false;
    }

    public function step4(Request $request){

        /**/

        $image_path = $request->input('image_path',false);

        //$imageName = $request->input('image_name', false);
        if(isset($_FILES) && count($_FILES)){
            if(isset($_FILES["img_cover"]["name"]) && $_FILES["img_cover"]["name"]){
                $uploadImg = $this->uploadImg();
                if(!$uploadImg["error"]){
                    $image_path = $uploadImg["file_url"];
                    //$imageName = $uploadImg["image_name"];
                }
            }
        }
        /**/

        $pageTemplate = $request->input('page_template');
        $imageName = $request->input('image_name');
        $bookId  = $request->input('book_id', false);
        $book = \App\Book::find($bookId);
        $pageNum = 1;
        if($book){
            $pages = $book->Pages;
            if(count($pages)){
                $pageNum = ($pages[count($pages)-1]->page_number) + 1;
            }
        }

        return view('step4', ['image_path' => $image_path, 'image_name' => $imageName,'page_template' => $pageTemplate,'pageNum' => $pageNum,"bookId" => $bookId]);
    }

    /**/

    public function loadCoverTemplates(){
        $tmp_files = scandir(base_path()."/resources/views/templates/cover");
        $files = array();
        for($i = 2; $i < count($tmp_files); $i++){
            $pos = strpos($tmp_files[$i],".blade.php");
            $name = substr($tmp_files[$i], 0, $pos);
            if($name){
                $files[] = $name;
            }
        }

        return $files;
    }

    public function loadPageTemplates(){
        $tmp_files = scandir(base_path()."/resources/views/templates/pages");
        $files = array();
        for($i = 2; $i < count($tmp_files); $i++){
            $pos = strpos($tmp_files[$i],".blade.php");
            $name = substr($tmp_files[$i], 0, $pos);
            if($name){
                $files[] = $name;
            }
        }

        return $files;
    }


    /**/
}
