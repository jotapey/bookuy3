<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Item;
use Illuminate\Support\Facades\DB;

class Book extends Model
{
    protected $table = 'books';

    public function items(){
        return $this->hasMany('\App\Model\Item');
    }

    public static function saveOrUpdate($data)
    {
        // Validate the request...


        $book = new Book();

        $book->author_id = $data->author_id;
        $book->status = $data->status;
        $book->title = $data->title;
        $book->author_name = $data->author_name;
        $book->ilustrador_name = $data->ilustrador_name;
        $book->css = $data->css;

        if($book->save()){
            return $book->id;
        }

        return false;
    }

    public static function getLibros($author_id,$getItems = true){

        $ret = array();
        $books = DB::table('books')->where([
            'author_id' => $author_id
        ])->get();

        foreach($books as $book){
            if($getItems){
				
				$items_tmp = array();
				
				foreach(DB::table('items')->where([
                        'book_id' => $book->id,
						'type' => 'cover',
                    ])->orderBy('order', 'ASC')->get() as $t){
						
						$items_tmp[] = $t;
				}
				
				foreach(DB::table('items')->where([
                        'book_id' => $book->id,
						'type' => 'page',
                    ])->orderBy('order', 'ASC')->get() as $t){
						
					$items_tmp[] = $t;
				}
				
				foreach(DB::table('items')->where([
                        'book_id' => $book->id,
						'type' => 'credits',
                    ])->orderBy('order', 'ASC')->get() as $t){
						
						$items_tmp[] = $t;
				}
				
				$ret[] = array(
                    "book" => $book,
                    "items" => $items_tmp);
                /*$ret[] = array(
                    "book" => $book,
                    "items" => DB::table('items')->where([
                        'book_id' => $book->id,
						'type' => 'cover'
                    ])->get());*/
            }else{
                $ret[] = array(
                    "book" => $book,
                    'book_id' => $book->id
                );
            }
        }

        return $ret;
    }

    public static function getLibro($book_id,$getItems = true){

        $ret = array();
        $books = DB::table('books')->where([
            'id' => $book_id
        ])->get();

        foreach($books as $book){
            if($getItems){
				$items_tmp = array();
				
				foreach(DB::table('items')->where([
                        'book_id' => $book->id,
						'type' => 'cover',
                    ])->orderBy('order', 'ASC')->get() as $t){
						
						$items_tmp[] = $t;
				}
				
				foreach(DB::table('items')->where([
                        'book_id' => $book->id,
						'type' => 'page',
                    ])->orderBy('order', 'ASC')->get() as $t){
						
					$items_tmp[] = $t;
				}
				
				foreach(DB::table('items')->where([
                        'book_id' => $book->id,
						'type' => 'credits',
                    ])->orderBy('order', 'ASC')->get() as $t){
						
						$items_tmp[] = $t;
				}
					
				/*echo "<pre>";
				print_r($items_tmp);
				die();*/
					
				/*$items_pages = DB::table('items')->where([
                        'book_id' => $book->id,
						'type' => 'pages',
                    ])->orderBy('order', 'ASC')->get();
					
				$items_credits = DB::table('items')->where([
                        'book_id' => $book->id,
						'type' => 'credits',
                    ])->orderBy('order', 'ASC')->get();
					
				$items_tmp = array_merge($items_cover, $items_pages);
				//$items_tmp = array_merge($items_tmp, $items_credits);
				
				echo "<pre>";print_r($items_tmp);die();*/
				
				$ret[] = array(
                    "book" => $book,
                    "items" => $items_tmp);
				
                /*$ret["items"][] = DB::table('items')->where([
                        'book_id' => $book->id,
						'type' => 'pages',
                    ])->orderBy('order', 'ASC')->get();
					
				$ret["items"][] = DB::table('items')->where([
                        'book_id' => $book->id,
						'type' => 'credits',
                    ])->orderBy('order', 'ASC')->get();*/
					
				
            }else{
                $ret[] = array(
                    "book" => $book,
                    'book_id' => $book->id
                );
            }
        }

        return $ret;
    }

    public static function delete_book($book_id){
        $ret = true;
        $book = DB::table('books')->where([
            'id' => $book_id
        ])->get();

        if($book){
            $pages = DB::table('items')->where([
                'book_id' => $book_id
            ])->get();

            if(count($pages)){
                foreach($pages as $page){
                    $delete = Item::delete_page($page->id);
                    if(!$delete){
                        $ret = false;
                        break;
                    }
                }
            }

            if($ret){
                $book = Book::find($book_id);

                if($book) {
                    $book->delete();
                    return true;
                }
            }

            return false;
        }
    }



}
