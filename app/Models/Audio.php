<?php
/**
 * Created by PhpStorm.
 * User: jotapey
 * Date: 12/4/2017
 * Time: 6:20
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Book;
use Illuminate\Support\Facades\DB;

class Audio extends Model
{
    protected $table = 'audio';

    public function book(){
        return $this->belongsTo('\App\Item');
    }

    public static function deleteAudio($id){
        return DB::table('audio')->where([
            ['id', '=', $id],
        ])->delete();
    }

    public static function getAll($where){
        return DB::table('audio')->where($where)->get();
    }

    public static function saveOrUpdate($data,$id=null)
    {
        // Validate the request...

        if(!$id) {
            $audio = new Audio();

            $order = DB::table('audio')->where('item_id', $data->item_id);

            $audio->id = $data->audio;
            $audio->item_id = $data->item_id;
            $audio->audio = $data->audio;
            $audio->loop = $data->loop;
            $audio->delay = $data->delay;

            if ($audio->save()) {
                return $audio->audio;
            }
        }else{
            $audio = DB::table('audio')->where([
                //['book_id', '=', $data->book_id],
                ['id', '=', $id],
            ])->get();

            if(count($audio)){
                $audio = $audio[0];
            }

            if(isset($data->loop) && $data->loop){
                $audio->loop = $data->loop;
            }

            if(isset($data->delay) && $data->delay){
                $audio->delay = $data->delay;
            }

            $audio->item_id = $data->item_id;

            $audio = (array)$audio;

            $update = DB::table('audio')->where([
                ['id', '=', $audio["id"]],
            ])->update($audio);

            return $audio["id"];
        }

        return false;
    }

}