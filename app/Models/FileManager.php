<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Item;
use App\Models\Book;
use File;

class FileManager extends Model
{

    public static function  createBookFile($path)
    {
        if(!File::exists($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
    }

    public static function  deleteBookFile($path)
    {
        if(!File::exists($path)) {
            return File::deleteDirectory($path);
        }
    }

}
