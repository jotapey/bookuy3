<?php
/**
 * Created by PhpStorm.
 * User: jotapey
 * Date: 12/4/2017
 * Time: 6:20
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Book;
use Illuminate\Support\Facades\DB;

class Item extends Model
{
    protected $table = 'items';

    public function book(){
        return $this->belongsTo('\App\Boook');
    }

    public static function deleteItem($id){
        return DB::table('items')->where([
            ['id', '=', $id],
        ])->delete();
    }

    public static function getAll($where){
        return DB::table('items')->where($where)->get();
    }

    public static function delete_page($page_id){
        //$page = DB::table('items')->where('id', $page_id);
        $page = Item::find($page_id);

        if($page){
            $page->delete();
            return true;
        }

        return false;
    }

    public static function saveOrUpdate($data)
    {
        // Validate the request...

        if(!$data->id) {
            $item = new Item();

            $count = Item::where(['book_id' => $data->book_id, 'type' => 'page'])->count();

            $item->id = $data->id;
            $item->type = $data->type;
            $item->texts = $data->texts;
            $item->images = $data->images;
            $item->book_id = $data->book_id;
            $item->status = $data->status;
            $item->order = $count + 1;
            $item->thumbnail = $data->images;
            $item->html = $data->html;


            if ($item->save()) {
                return $item->id;
            }
        }else{
            $item = DB::table('items')->where([
                //['book_id', '=', $data->book_id],
                ['id', '=', $data->id],
            ])->get();

            if(count($item)){
                $item = $item[0];
            }


            $updateItem = [
                'texts' => $data->texts,
                'status' => $data->status,
                'html' => $data->html,
            ];

            if($data->images){
                $updateItem['images'] = $data->images;
                $updateItem['thumbnail'] = $data->images;
            }

            $update = DB::table('items')->where([
                ['book_id', '=', $item->book_id],
                ['id', '=', $item->id],
            ])->update($updateItem);

            return $item->id;
        }

        return false;
    }

    public static function updateOrder($item_id, $order){
        $updateItem = ['order' => $order];

        $update = DB::table('items')->where([
            ['id', '=', $item_id],
        ])->update($updateItem);

        return $update;
    }

}