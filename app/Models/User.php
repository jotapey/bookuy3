<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

/**
 * @property string $name
 * @property string $surname
 * @property string $username
 * @property string $email
 * @property string $password
 * @property bool $activated
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'user_role', 'user_id', 'role_id');
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles))
        {
            foreach ($roles as $role)
            {
                if ($this->hasRole($role)){
                    return true;
                }
            }
        }
        else
        {
            if ($this->hasRole($roles))
            {
                return true;
            }
        }
    }

    public function setUserRoleSession(){
        $role = null;

        if(Auth::id()){
            $role = DB::table('users')
                ->join('user_role', 'user_role.user_id', '=', 'users.id')
                ->join('roles', 'roles.id', '=', 'user_role.role_id')
                ->select('roles.name')
                ->where('users.id', '=', Auth::id())
                ->get();

            Session::set('userRole', $role[0]->name );
        }
    }

    public function hasRole($role)
    {

        $this->setUserRoleSession();

        if ($role === '*')
        {
            return true;
        }

        if ($this->roles()->where('name', $role)->first())
        {
            return true;
        }

        return false;
    }
}
