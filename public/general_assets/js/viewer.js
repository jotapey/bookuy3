jQuery(document).ready(function(){
    var itera_pages = 0;
    playAudio();

    jQuery(document).on("mousedown", "#bb-nav-first", function(){
        itera_pages = 0;
        current_page = pages[itera_pages];
        playAudio();
    });
    jQuery(document).on("mousedown", "#bb-nav-prev", function(){
        if(itera_pages>0){
            itera_pages--;
        }
        current_page = pages[itera_pages];
        playAudio();
    });
    jQuery(document).on("mousedown", "#bb-nav-next", function(){
        if( itera_pages < (pages.length -1 )) {
            itera_pages++;
        }
        current_page = pages[itera_pages];
        playAudio();
    });
    jQuery(document).on("mousedown", "#bb-nav-last", function(){
        itera_pages = pages.length -1;
        current_page = pages[itera_pages];
        playAudio();
    });
});

var media_audios = [];

function playAudio(){
    resetAudios();
    media_audios = [];
    for(var i in audios){
        paudio = audios[i];
        src = baseUrl+"books" + DIRECTORY_SEPARATOR + "audios" + DIRECTORY_SEPARATOR + paudio.audio+".mp3";
        var audio = new Audio(src);
        if(paudio.item_id == current_page){

            if(paudio.loop != "no_repeat"){
                audio.loop = true;
            }

            media_audios.push(audio);
            media_audios[media_audios.length-1].play();

        }else{
            if(paudio.loop == "all_book"){
                playable_pages = getPagesPlay(paudio);
                if(in_array(current_page,playable_pages)){
                    audio.loop = true;
                    media_audios.push(audio);
                    media_audios[media_audios.length-1].play();
                }
            }
        }
    }
}

function resetAudios(){
    for(var i in media_audios){
        a = media_audios[i];
        a.pause();
        a.currentTime = 0;
    }
}

function getPagesPlay(audio){
    var playable_pages = [];
    find = false;
    for(var i in pages){
        if(pages[i] == audio.item_id || find){
            find = true;
            playable_pages.push(pages[i]);
        }
    }

    return playable_pages;
}

function in_array(aguja, pajar){
    for(var i in pajar){
        if(pajar[i] == aguja){
            return true;
        }
    }

    return false;
}

