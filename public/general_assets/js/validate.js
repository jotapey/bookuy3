function validate(data,validations){
    var ret = {
        "success": "Ok",
        "error":"Error en el formulario",
        "messages": [],
        "error": false
    };

    resetErrorFormMsg(validations);

    for(var i in validations){
        if(validations[i].required){
            if(!data[i]){
                ret.error = true;
                ret.messages.push(validations[i].onError.msg);
                errorMsg = '<span class="help-block">'+validations[i].onError.msg+'</span>';
                jQuery(validations[i].selector).parent().append(errorMsg);
                jQuery(validations[i].selector).parent().addClass("has-error");
            }
        }
    }

    return ret;
}

function resetErrorFormMsg(validations){
    jQuery(document).find(validations.wrapper).find(".help-block").each(function(){jQuery(this).remove()})
    jQuery(validations.wrapper+" .form-group").removeClass("has-error");
    jQuery(validations.wrapper+" .styles-group").removeClass("has-error");
}