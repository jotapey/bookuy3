<style>
    .cropit-preview-image-container{
        width: 400px !important;
        height: 400px !important;
    }
    .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 1px solid #ccc;
        border-radius: 3px;
        width: 400px !important;
        height: 400px !important;
    }

    .cropit-preview-image-container {
        cursor: move;
    }

    .image-size-label {
        margin-top: 10px;
    }

    input, .export {
        display: block;
    }

    button {
        margin-top: 10px;
    }
    .cover-inputs.form-group{
        position: absolute;
        width: 600px;
        margin-top: 50px;
        margin-left: 100px;
        z-index: 1;
    }
    .cover-inputs.form-group input{
        margin-bottom: 20px;
        text-align: center;
    }
    .cropit-right{
        position: relative;
        width: 400px;
        float: left;
        height: 400px;
    }
    .image-editor-wapper-left .form-group-textarea {
        width: 400px;
        height: 400px;
        margin-top: 0px;
        position: relative;
        float: left !important;
        left: 0;
    }
    .image-editor-wapper-left .cropit-preview.cropit-image-loaded {
        left: 0;
    }
</style>
<div class="image-editor">
    <input type="file" class="cropit-image-input">
    <div class="image-editor-wapper image-editor-wapper-left image-editor-wapper-page">
        <div class="form-group form-group-textarea">
            <div class="text-vertical-middle-allign">
                <div class="div-editable" style="display: table-cell; vertical-align: middle" contenteditable="true"></div>
            </div>
            <!--<textarea class="form-group"></textarea>-->
        </div>
        <div class="cropit-preview cropit-right"></div>
    </div>
    <div class="image-size-label">
        Redimensionar imagen
    </div>
    <input type="range" class="cropit-image-zoom-input">
</div>
<script>
    $(function() {
        $('.image-editor').cropit({
            smallImage:'stretch',
            minZoom: 'fill',
            maxZoom:2
        });

    });

    $("div.div-editable").on('keydown paste', function (event) {
        if ($(this).text().length === cntMaxLength && event.keyCode != 8) {
            event.preventDefault();
        }
    });
</script>
