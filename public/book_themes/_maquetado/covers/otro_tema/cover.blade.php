
<input placeholder="Titulo" type="text" id="title" class="form-control title-cover">
<input placeholder="Autor" type="text" id="author" class="form-control title-cover" style="top: 140px;">
<div id="image-preview">
    <label for="image-upload" id="image-label">Seleccionar Imagen</label>
    <input type="file" name="image" id="image-upload" />
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $.uploadPreview({
            input_field: "#image-upload",
            preview_box: "#image-preview",
            label_field: "#image-label",
            label_default: "Seleccionar Imagen",
            label_selected: "Seleccionar Imagen",
            no_label: false
        });
    });
</script>
<style>
    #image-preview {
        width: 800px;
        height: 400px;
        position: relative;
        overflow: hidden;
        background-color: #ffffff;
        color: #ecf0f1;
    }
    #image-preview input {
        line-height: 200px;
        font-size: 200px;
        position: absolute;
        opacity: 0;
        z-index: 10;
    }
    #image-preview label {
        position: absolute;
        z-index: 5;
        opacity: 0.8;
        cursor: pointer;
        background-color: #bdc3c7;
        width: 280px;
        height: 50px;
        font-size: 20px;
        line-height: 50px;
        text-transform: uppercase;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        text-align: center;
    }
</style>
