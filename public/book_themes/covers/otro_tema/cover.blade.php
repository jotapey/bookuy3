<style>
    .cropit-preview-image-container{
        width: 800px !important;
        height: 400px !important;
    }
    .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 800px !important;
        height: 400px !important;
    }

    .cropit-preview-image-container {
        cursor: move;
    }

    .image-size-label {
        margin-top: 10px;
    }

    input, .export {
        display: block;
    }

    button {
        margin-top: 10px;
    }
    .cover-inputs.form-group{
        position: absolute;
        width: 600px;
        margin-top: 50px;
        margin-left: 100px;
        z-index: 1;
    }
    .cover-inputs.form-group input{
        margin-bottom: 20px;
        text-align: center;
    }
</style>
<div class="image-editor">
    <input type="file" class="cropit-image-input">
    <div class="image-editor-wapper image-editor-wapper-cover">
        <div class="cover-inputs form-group">
            <input placeholder="Titulo" type="text" id="title" class="form-control">
            <input placeholder="Autor" type="text" id="author" class="form-control" >
            <input placeholder="Ilustrador" type="text" id="ilustrador" class="form-control" >
        </div>
        <div class="cropit-preview"></div>
    </div>
    <div class="image-size-label">
        Redimensionar imagen
    </div>
    <input type="range" class="cropit-image-zoom-input">
    <!--<button class="rotate-ccw">Rotate counterclockwise</button>
    <button class="rotate-cw">Rotate clockwise</button>

    <button class="export">Export</button>-->
</div>

<script>
    $(function() {
        $('.image-editor').cropit({
            smallImage:'stretch',
            minZoom: 'fill',
            maxZoom:2
        });

    });
</script>
