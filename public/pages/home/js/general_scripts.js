jQuery(document).ready(function(){

    jQuery(".generate-apk").click(function(e){
        e.preventDefault();
        _this = jQuery(this);
        var book_id = jQuery(this).attr("book_id");
        var author = jQuery(this).attr("author");
        
        $.ajax({
            type: "POST",
            url: baseUrl+"ajax/make-apk",
            dataType: 'text',
            data: {
                book_id : book_id,author:author
            },
            success:function(data){
                location.reload();
            }
        });
    })

    jQuery(".btn-danger").click(function(e){
        e.preventDefault();
        _this = jQuery(this);
        var book_id = jQuery(this).attr("book_id");
        $.confirm({
            title: 'Seguro de eliminar el libro?',
            content: '',
            buttons: {
                confirm: function () {
                    $.ajax({
                        type: "POST",
                        url: baseUrl+"ajax/delete-book",
                        dataType: 'text',
                        data: {
                            book_id : book_id
                        },
                        success:function(data){
                            data = JSON.parse(data);
                            if(data.success == true){
                                $.alert('Libro eliminado!');
                                _this.parent().fadeOut();
                            }else{
                                $.alert('Error al eliminar el libro!');
                            }
                        }
                    });
                },
                cancel: function () {
                },
            }
        });
    })
})