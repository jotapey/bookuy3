var rec;
var audiosrc = null;
var recordedAudio;
var blob = null;

navigator.mediaDevices.getUserMedia({audio:true})
    .then(stream => {
    rec = new MediaRecorder(stream);
rec.ondataavailable = e => {
    audioChunks.push(e.data);
    if (rec.state == "inactive"){
        recordedAudio = jQuery(document).find("#recordedAudio");
        if(recordedAudio.length){
            recordedAudio = recordedAudio[0];
        }
        blob = new Blob(audioChunks,{type:'audio/x-mpeg-3'});
        recordedAudio.src = URL.createObjectURL(blob);
        audiosrc = recordedAudio.src;
        recordedAudio.controls=true;
        recordedAudio.autoplay=false;
    }
}
}).catch(e=>console.log(e));

$(document).ready(function() {

    jQuery(document).on("click", ".remove_audio", function(){
        jQuery(this).parent().parent().remove();
    });

    jQuery(document).on("click", ".btn-save-audio", function(){
        o = jQuery("input[name=audio_origin]:checked").val();
        total_rows = jQuery(".audios-table tbody tr").length+1;
        if(o == "record"){
            audio = jQuery("#modalAudio").find("audio").clone().prop('name', 'files_audio[]' ).prop('id', 'new_record' );
            /**/
            var audio_file = new FormData();
            tmp_blob = blob;

            if(blob == null){
                $.alert("Detenga la grabación");
                return false;
            }

            if(blob){
                tmp_blob.name = "blobaudio.mp3";
                audio_file.append('audio_file', blob);
                //audio_file.append('author_id', book_active.author_id);

                audio_file.append('item_id', null);
                audio_file.append('id', null);
                audio_file.append('loop', "no_repeat");
                audio_file.append('delay', "0");

                jQuery.ajax({
                    url: baseUrl+"ajax/upload-pre-audio",
                    data: audio_file,
                    type: "POST",
                    async: false,
                    enctype: 'multipart/form-data',
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,
                    success: function (data) {
                        if(data.status == "success"){

                            var audio = new Audio();
                            audio.controls = true;
                            audio.src = data.url_audio.url;
                            audio.name = "files_audio[]";
                            audio.id = data.url_audio.id;

                            row = '';
                            row += '<tr class="audio_append last">';
                            row += '<td class="col-xs-1"></td>';
                            row += '<td class="col-xs-1"><input type="text" class="delay_seconds" class="form-control" placeholder="Segundos" value="0"></td>';
                            row += '<td class="col-xs-1"><input type="radio" name="loop_row'+total_rows+'" checked value="no_repeat"></td>';
                            row += '<td class="col-xs-1"><input type="radio" name="loop_row'+total_rows+'" value="current_page"></td>';
                            row += '<td class="col-xs-1"><input type="radio" name="loop_row'+total_rows+'" value="all_book"></td>';
                            row += '<td class="col-xs-1"><button class="btn btn-danger remove_audio">Eliminar</button></td>';
                            row += '</tr>';

                            jQuery(".tab-pane.active #audios .audios-table tbody").append(row);

                            last = jQuery(".tab-pane.active #audios .audio_append.last");
                            td = last.find("td:first");
                            td.append(audio);
                            last.removeClass("last");

                            audio_file = {
                                "id":audiosrc,
                                "repeat":jQuery("input[name=loop]:checked").val(),
                                "delay": jQuery(".delay_seconds").val(),
                            };
                            audio_files.push(audio_file);

                            jQuery("#modalAudio").find("audio").remove();

                            jQuery("#modalAudio .record-file p:last").append('<audio id="recordedAudio"></audio>');
                        }
                    }
                });
            }
                        /**/


        }else{

            var audio_file = new FormData();

            current_audio = jQuery(document).find("#modalAudio input[type=file]");
            audio_file.append('audio_file', current_audio[0].files[0]);
            //audio_file.append('author_id', book_active.author_id);
            audio_file.append('item_id', null);
            audio_file.append('id', null);
            audio_file.append('loop', "no_repeat");
            audio_file.append('delay', "0");
            audio = false;
            jQuery.ajax({
                url: baseUrl+"ajax/upload-pre-audio",
                data: audio_file,
                type: "POST",
                async: false,
                enctype: 'multipart/form-data',
                processData: false,  // tell jQuery not to process the data
                contentType: false,
                success: function (data) {
                    if(data.status == "success"){

                        var audio = new Audio();
                        audio.controls = true;
                        audio.src = data.url_audio.url;
                        audio.name = "files_audio[]";
                        audio.id = data.url_audio.id;
                        audio.controls

                        row = '';
                        row += '<tr class="audio_append last">';
                        row += '<td class="col-xs-1"></td>';
                        row += '<td class="col-xs-1"><input type="text" class="delay_seconds" class="form-control" placeholder="Segundos" value="0"></td>';
                        row += '<td class="col-xs-1"><input type="radio" name="loop_row'+total_rows+'" checked value="no_repeat"></td>';
                        row += '<td class="col-xs-1"><input type="radio" name="loop_row'+total_rows+'" value="current_page"></td>';
                        row += '<td class="col-xs-1"><input type="radio" name="loop_row'+total_rows+'" value="all_book"></td>';
                        row += '<td class="col-xs-1"><button class="btn btn-danger remove_audio">Eliminar</button></td>';
                        row += '</tr>';

                        jQuery(".tab-pane.active #audios .audios-table tbody").append(row);
                        last = jQuery(".tab-pane.active #audios .audio_append.last");
                        td = last.find("td:first");
                        td.append(audio);
                        last.removeClass("last");
                    }
                    jQuery(document).find("#modalAudio input[type=file]").remove();
                    jQuery(document).find("#modalAudio .upload-file").append('<form id="audioupload" action="#" method="POST" enctype="multipart/form-data"><input type="file" class="form-control"></form>');
                }
            });
        }

        jQuery("#modalAudio").modal("hide");
    });

    jQuery(document).on("change", "input[name=audio_origin]",function(){
        source = jQuery(this).val();

        if(source == "upload"){
            jQuery(".form-group.upload-file").removeClass("hidden");
            jQuery(".form-group.record-file").addClass("hidden");
        }else{
            if(source == "record"){
                jQuery(".form-group.upload-file").addClass("hidden");
                jQuery(".form-group.record-file").removeClass("hidden");
            }
        }
    });

    jQuery(document).on("click", "#mstartRecord", function(e){
        console.log("start");
        jQuery(this).attr("disabled", true);
        jQuery("#stopRecord").attr("disabled", false);
        audioChunks = [];
        rec.start();
    });
    jQuery(document).on("click", "#stopRecord", function(e){
        console.log("stop");
        recordedAudio = jQuery(document).find("#modalAudio .record-file #recordedAudio");
        jQuery("#mstartRecord").attr("disabled", false);
        jQuery(this).attr("disabled", true);
        rec.stop();
    });

    jQuery(document).on("click", ".play-all", function(){
        jQuery(this).removeClass("play-all");
        jQuery(this).addClass("stop-all");
        jQuery(this).text("Stop");
        audios = [];
        jQuery("#audios .audios-table tr.audio_append").each(function(){
            audio_element = jQuery(this).find("audio");
            audios.push({
                audio: audio_element,
                delay:audio_element.parent().next().find("input").val(),
            });
        });

        var x = 0;
        higger = 0;

        var interval = setInterval(function() {
            for(var i in audios){
                a = audios[i].audio[0];
                delay = parseInt(audios[i].delay)*1000;
                if(delay > higger){
                    higger = delay;
                }
                if(delay <= x){
                    a.play();
                }
            }
            if(x >= higger){
                clearInterval(interval);
            }
            x += 1000;
        }, 1000);
    });

    jQuery(document).on("click", ".stop-all", function(){
        jQuery(this).removeClass("stop-all");
        jQuery(this).addClass("play-all");
        jQuery(this).text("Play");
        audios = [];
        jQuery("#audios .audios-table tr.audio_append").each(function(){
            audio_element = jQuery(this).find("audio");
            audios.push({
                audio: audio_element,
                delay:audio_element.parent().next().find("input").val(),
            });
        });

        for(var i in audios){
            a = audios[i].audio[0];
            a.pause();
            a.currentTime = 0;
        }
    });
});


