
jQuery(document).ready(function(){

    var showDialog = false;

    jQuery(document).on("mousedown", ".dz-clickable", function(e){
        if(!showDialog){
            e.preventDefault();

            var button = $(e.relatedTarget) // Button that triggered the modal
            var recipient = button.data('whatever')

            jQuery("#imagesSourceModal").modal('show');
            return false;
        }
    })

    jQuery(document).on("click", "#imagesSourceModal .btn-primary", function(){
        jQuery(".dz-clickable").trigger("click");
    })

    jQuery(document).on("click", "#imagesSourceModal .btn-default", function(){
        jQuery("#galleryModal").modal('show');
    })

    /**/
    jQuery(document).on("click", ".pages-preview-panel img, .panel-collapse img", function(){
        $(".overlay").show();
        setTimeout(function(){
            $(".overlay").hide();
        },2000)
    });

    jQuery(document).on("click",".btn-add-item",function(){
        jQuery("#modalItem").modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    jQuery(document).on("click","#add_audio",function(){
        jQuery("#modalAudio").modal("show");

    });
});