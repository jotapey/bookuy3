function loadTheme(){
    jQuery("#title").val( book_active.title );
    jQuery("#author").val( book_active.author_name );
}


function createNewBook(){
    jQuery.ajax({
        url: baseUrl+"ajax/save-or-update-book",
        data:{item_data:book_active},
        type:"post",
        async:false,
        success:function(data){
            if(data.status == "success"){
                console.log("ok: "+data.status);

                book_active = {
                    id:data.book_active.id,
                    author_id:data.book_active.author_id,
                    status:data.book_active.status,
                    title:data.book_active.title,
                    author_name:data.book_active.author_name,
                    ilustrador_name:data.book_active.ilustrador_name,
                    css:data.book_active.css
                }

                loadTheme();

                return;
            }
        },
        error:function(data){

        },
    })
}

function appendCss(data_item){

    jQuery(".work-panel link").each(function(){
        if(jQuery(this).attr("href")){
            data_item.css.push(jQuery(this).attr("href"));
            jQuery("#loaded-css-links").append("<link rel='stylesheet' href='" +  jQuery(this).attr("href") + "' >");
        }
        jQuery(this).remove();
    });

    setBookActive(data_item);
}