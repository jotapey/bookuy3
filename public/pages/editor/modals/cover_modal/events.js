jQuery(document).ready(function(){

    //Click en la imagen de seleccionar un tema
    /*jQuery(document).on("click", ".img-book-style-css", function(){
        var css = jQuery(this).parent().find("input[type=hidden]").attr("value");
        var base_url = jQuery(document).find("#base_url").val();
        jQuery(".img-book-style-css").css({
            "border": "none",
            "width":"100%"
        });
        jQuery(".img-book-style-css").removeClass("img-book-style-css-selected");

        jQuery(this).css({
            "border": "4px solid skyblue",
            "width":"97%"
        });

        jQuery(this).addClass("img-book-style-css-selected");

        jQuery(document).find("head").append("<link rel='stylesheet' href='"+base_url+css+"'></link>");
        //$('#myModal button.close').trigger("click");
    });*/

    jQuery(document).on("click", ".selector-item-cover", function (e) {
        e.preventDefault();
        active_item.id = jQuery(this).attr("item_id");
        active_item.item_selected = jQuery(this);
    });

    jQuery(".btn-save-cover").click(function(){
        id = active_item.id;
        jQuery(".btn-guardar-item").attr("disabled",false);
        jQuery.ajax({
            url: baseUrl+"ajax",
            data:{id:id,type:"covers"},
            type:"post",
            success:function(data){

                data_item = {
                    author_id:current_user,
                    title:jQuery("input[name=book-title]").val(),
                    author_name:jQuery("input[name=book-author]").val(),
                    ilustrador_name:jQuery("input[name=book-ilustrador]").val(),
                    item_selected:active_item.item_selected,
                    css:[],
                    items:[
                        {
                            type: "cover",
                            html: jQuery(".work-panel .active.tab-pane-workarea").html(),
                            image:"",
                        }
                    ],
                };

                active_item.title = data_item.title;
                if(data_item.ilustrador_name){
                    active_item.ilustrador_name = data_item.ilustrador_name;
                }else{
                    active_item.ilustrador_name = data_item.author_name;
                }

                active_item.author_name = data_item.author_name;

                valid = validate(
                    data_item,{
                        "wrapper": "#myModal",
                        "title": {
                            "selector": "input[name=book-title]",
                            "required":true,
                            "maxlength":250,
                            "onError":{
                                "msg": "El campo titulo es requerido",
                            }
                        },
                        "author_name": {
                            "selector": "input[name=book-author]",
                            "required":true,
                            "maxlength":250,
                            "onError":{
                                "msg": "El campo Autor es requerido",
                            }
                        },
                        "item_selected": {
                            "selector": "a.selector-item-cover",
                            "required":true,
                            "onError":{
                                "msg": "Seleccione un diseño",
                            }
                        },
                    }
                );

                if(!valid.error) {

                    jQuery(".tab-pane-workarea").html(data.html);
                    css_path = active_item.item_selected.next().attr("css_path");
                    jQuery(".tab-pane-workarea").append("<link rel='stylesheet' href='" + css_path + "' />");
                    appendCss(data_item);
                    createNewBook();
                    $('#myModal button.close').trigger("click");
                }


            },
            error:function(data){

            },
        });



    });

});