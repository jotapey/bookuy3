function loadHtmlItemSaved(){
    jQuery(".overlay").show();
    try{
        if(viewer) {
            jQuery(".tab-pane-workarea").fadeOut();
        }
    }catch(e){

    }

    jQuery.ajax({
        url: baseUrl+"ajax/load-html-item-saved",
        data:{selected_item:selected_item},
        type:"post",
        success:function(data){
            //data = JSON.parse(data);
            var html = "";
            if(data.status == "success"){
                html = data.html;
				html = html.replace(/(?:\r\n|\r|\n)/g, '');

                if(html){
                    console.log(html);
                    jQuery(".tab-pane-workarea").html(html);
                    jQuery(".tab-pane-workarea").find("textarea").each(function(){
                        var text = jQuery(this).attr("value");
                        jQuery(this).val(text)
                    });

                    jQuery(".tab-pane-workarea").find("input[type=text]").each(function(){
                        var text = jQuery(this).attr("value");
                        jQuery(this).val(text)
                    });
                    try{
                        if(viewer) {
                            jQuery(".tab-pane-workarea").fadeIn();
                            jQuery(".overlay").hide();
                        }
                        if(editor){
                            jQuery(".overlay").hide();
                        }
                    }catch(e){

                    }

                    if(data.audios){
                        jQuery(".audios-table tbody").html("");
                        audios = data.audios;
                        total_rows = 1;
                        for(var i in audios){

                            var audio = new Audio();
                            audio.controls = true;
                            audio.src = audios[i].audio;
                            audio.name = "files_audio[]";
                            audio.id = audios[i].id;

                            row = '';
                            row += '<tr class="audio_append last">';
                            row += '<td class="col-xs-1"></td>';
                            row += '<td class="col-xs-1"><input type="text" class="delay_seconds" class="form-control" placeholder="Segundos" value="0"></td>';
                            row += '<td class="col-xs-1"><input type="radio" name="loop_row'+total_rows+'" checked value="no_repeat"></td>';
                            row += '<td class="col-xs-1"><input type="radio" name="loop_row'+total_rows+'" value="current_page"></td>';
                            row += '<td class="col-xs-1"><input type="radio" name="loop_row'+total_rows+'" value="all_book"></td>';
                            row += '<td class="col-xs-1"><button class="btn btn-danger remove_audio">Eliminar</button></td>';
                            row += '</tr>';
                            total_rows++;

                            jQuery(".audios-table tbody").append(row);

                            last = jQuery(".audio_append.last");
                            td = last.find("td:first");
                            td.append(audio);
                            last.removeClass("last");
                        }

                    }

                }
            }
            $("input[type=range]").val(0);
            jQuery(".overlay").hide();
            console.log(data)
        },
        error:function(data){
            jQuery(".overlay").hide();
        },
    })
}

function saveOrUpdateAudioItem(item_id){
    $(".overlay").show();
    send_data = [];
    itera = 1;
    jQuery(".audios-table tbody tr").each(function(){
        tmpaudio = jQuery(this).find("audio");
        audio = {};
        audio.id= tmpaudio.attr("id");
        audio.delay= jQuery(this).find(".delay_seconds").val();
        audio.loop= jQuery(this).find("input[name=loop_row"+itera+"]:checked").val();
        send_data.push(audio);
        itera++;
    });

    jQuery.ajax({
        url: baseUrl + "ajax/save-or-update-audio",
        data:{send_data:send_data,book_id:book_active.id, item_id:item_id},
        type: "POST",
        async: false,
        success: function (data) {

        }
    });

}

function saveOrUpdateItem(){
	$(".overlay").show();
    var type = "cover";
    var id = selected_item.item_id;

    //selected_item
    active_item.type = "page";

    if(!jQuery(".pages-preview-panel canvas").length){
        active_item.type = "cover";
    }

    var current_item = new FormData();

    current_item.append('image-0', jQuery('.cropit-preview-image-container img').attr("src"));

    for(var i in active_item){
        tmpElement = active_item[i];
        if(active_item[i].isArray || (typeof active_item[i] === 'object')){
            tmpElement = JSON.stringify(active_item[i]);
        }
        current_item.append(i, tmpElement);
    }

    jQuery(".tab-pane-workarea").find("textarea").each(function(){
        jQuery(this).attr("value", jQuery(this).val());
    })

    jQuery(".tab-pane-workarea").find("input[type=text]").each(function(){
        jQuery(this).attr("value", jQuery(this).val());
    });

    current_item.append('id', id);
    current_item.append('book_id', book_active.id);
    current_item.append('author_id', book_active.author_id);
    current_item.append('css', book_active.css);
    current_item.append('html', jQuery(".tab-pane-workarea").html());

    jQuery.ajax({
        url: baseUrl+"ajax/save-or-update-item",
        //type:"post",
        data:current_item,
        type: "POST",
		async: false,
        enctype: 'multipart/form-data',
        processData: false,  // tell jQuery not to process the data
        contentType: false,
        success:function(data){

            if(data.status != "error"){

                active_item.item_id = data.active_item.id;
                active_item.book_id = data.active_item.book_id;

                html2canvas(jQuery(".image-editor-wapper"), {
                    onrendered: function(canvas) {

                        total = jQuery(".pages-preview-panel .panel-body-horizontal canvas").length;

                        jQuery(document).find("canvas.selected").removeClass("selected");

                        var cerrar = "<button class='eliminar-canvas'>x</button>";

                        if(total == 0){
                            type= "cover";
                            //jQuery("#preview_cover").remove();
                            //jQuery("#thumbnail_cover").remove();
                            canvas.setAttribute("id","thumbnail_cover")
                            canvas.setAttribute("item_id",active_item.item_id);
                            canvas.setAttribute("book_id",active_item.book_id);
                            canvas.setAttribute("class","selected");
                            canvas.setAttribute("type",type);

                            //jQuery(".panel-body-horizontal").prepend("<div class='canvas-container'></div>");
                            $(".owl-carousel")
                                .trigger('remove.owl.carousel', [1])
                                .trigger('add.owl.carousel', ["<div class='canvas-container'></div>"]).trigger('refresh.owl.carousel');
                            jQuery(".canvas-container").append(canvas);


                        }else{
                            type= "page";
                            canvas.setAttribute("id","thumbnail_cover");
                            canvas.setAttribute("item_id",active_item.item_id);
                            canvas.setAttribute("book_id",active_item.book_id);
                            canvas.setAttribute("class","selected");
                            canvas.setAttribute("type",type);

                            if(!id){
                                canvas.setAttribute("pos",getCountPages()+1);
                                //jQuery(".panel-body-horizontal").append("<div class='canvas-container'></div>");
                                $('.owl-carousel').trigger('add.owl.carousel', ["<div class='canvas-container'></div>"]).trigger('refresh.owl.carousel');
                                jQuery(".panel-body-horizontal .canvas-container:last").append(cerrar);
                                jQuery(".panel-body-horizontal .canvas-container:last").append(canvas);
                            }else{
                                indexToAppend = 0;
                                jQuery(".panel-body-horizontal").find("canvas").each(function(){
                                    if(id == jQuery(this).attr("item_id")){

                                        canvas.setAttribute("pos",jQuery(this).attr("pos"));

                                        var _this = jQuery(this);
                                        _this.parent().append(canvas);
                                        _this.remove()

                                        /*$('.owl-carousel').trigger('add.owl.carousel', ["<div class='canvas-container'></div>"], indexToAppend ).trigger('refresh.owl.carousel');

                                        jQuery(".panel-body-horizontal .canvas-container-new").append(cerrar);
                                        jQuery(".panel-body-horizontal .canvas-container-new").append(canvas);

                                        $('.owl-carousel').trigger( 'remove.owl.carousel', [indexToAppend] ).trigger('refresh.owl.carousel');

                                        //jQuery(".canvas-container-new").parent().remove();

                                        //jQuery(this).parent().parent().prev().after( added_item );

                                        //jQuery(this).parent().parent().remove();

                                        /*c = jQuery(".panel-body-horizontal").find(".canvas-container:last");
                                        c.insertAfter(jQuery(this).parent().prev());
                                        jQuery(this).parent().remove();*/

                                        /*jQuery(".owl-stage-outer .owl-stage .owl-item").each(function(){
                                            var ele = jQuery(this);
                                            c = ele.find(".canvas-container");
                                            if(!c.length){
                                                jQuery(this).remove();
                                            }
                                        });*/
                                    }
                                    indexToAppend++;
                                })
                            }

                            var w = 0;
                            w = jQuery(".owl-stage .owl-item").length * 204;
                            jQuery(".owl-stage").css("width",w);
                        }

                        selected_item = {
                            type:type,
                            book_id:active_item.book_id,
                            item_id:active_item.item_id
                        }
                        jQuery(".btn-guardar-item").attr("disabled", false);

                    },
                    width: 800,
                    height: 400
                });
                /*Fin viejo*/

                saveOrUpdateAudioItem(active_item.item_id);

                $( '#image-label' ).attr( 'style', 'display:block' );
                jQuery(".title-cover").css({
                    "background-color": "#fff",
                    "border": "1px solid #ccd0d2",
                    "box-shadow": $box_shadow
                });
            }
        },
        error:function(data){

        },
    })

}

jQuery(document).on("click", "canvas", function(){
    jQuery(document).find("canvas.selected").removeClass("selected");
    jQuery(this).addClass("selected");

    selected_item = {
        type:jQuery(this).attr("type"),
        book_id:jQuery(this).attr("book_id"),
        item_id:jQuery(this).attr("item_id")
    }

    loadHtmlItemSaved();

});

jQuery(document).on("click", ".eliminar-canvas", function(){
    var span = jQuery(this).parent();
    var item_id = span.find("canvas").attr("item_id");
    console.log("item_id: "+item_id);

    jQuery.ajax({
        url: baseUrl+"ajax/delete-item",
        data:{item_id:item_id},
        type:"post",
        success:function(data){
            if(data.status == "success"){

                var pos = 0;
                jQuery(".canvas-container").each(function(){
                    if(jQuery(this).find("canvas").attr("item_id") == item_id){
                        console.log("pos: "+pos);
                        $(".owl-carousel")
                            .trigger('remove.owl.carousel', [pos])
                            .trigger('refresh.owl.carousel');
                    }
                    pos++
                });

                //span.parent().fadeOut();
                //span.parent().remove();

                var w = 0;
                w = jQuery(".owl-stage .owl-item").length * 204;
                jQuery(".owl-stage").css("width",w);


            }
        },
        error:function(data){

        },
    })
});

jQuery(document).on("click", ".btn-right-page", function(){
    if(jQuery("canvas.selected").length){
        if(jQuery("canvas.selected").attr("type") == "page"){
            var total_canvas_pages = jQuery("canvas[type=page]").length;
            var pos = jQuery("canvas.selected").attr("pos");
            if(pos < total_canvas_pages){
                var tmpPos = getElementPosition("canvas", ".owl-stage-outer .owl-stage", "pos", pos)

                var element = jQuery("canvas.selected").parent().parent();

                element.remove();
                $(".owl-stage-outer .owl-stage .owl-item:eq("+(tmpPos)+")").after(element);
                updateAllItemPos();
            }
        }
    }
})

jQuery(document).on("click", ".btn-left-page", function(){
    if(jQuery("canvas.selected").length){
        if(jQuery("canvas.selected").attr("type") == "page"){
            var pos = jQuery("canvas.selected").attr("pos");
            if(pos != "1"){
                var tmpPos = getElementPosition("canvas", ".owl-stage-outer .owl-stage", "pos", pos)

                var element = jQuery("canvas.selected").parent().parent();
                var element_tmp = element.clone();

                element.remove();
                //var prev_element = element.prev();
                //var prev_element_tmp = prev_element.clone();

                $(".owl-stage-outer .owl-stage .owl-item:eq("+(tmpPos-1)+")").before(element);
                updateAllItemPos();
            }

        }
    }
});

function getElementPosition(element, container, attribute, id_attribute){
    pos = 0;
    find = false;
    jQuery(container).find(element).each(function(){
        if( jQuery(this).attr(attribute) == id_attribute){
            find = true;
        }

        if(!find){
            pos++;
        }
    })

    return pos;
}

function loadItemTheme(){

    var selected = active_item.item_selected;
    jQuery(".btn-guardar-item").attr("disabled",true);
    var id = active_item.id;
    jQuery.ajax({
        url: baseUrl+"ajax",
        data:{id:id,type:"pages"},
        type:"post",
        success:function(data){
            jQuery(".tab-pane-workarea").html(data.html);
            css_path = selected.next().attr("css_path");
            if(typeof css_path !== "undefined"){
                jQuery(".tab-pane-workarea").append("<link rel='stylesheet' href='"+css_path+"' />");
            }

            jQuery(".btn-guardar-item").attr("disabled",false);
            jQuery(".audios-table tbody").html("");
        },
        error:function(data){

        },
    })
}

function updatePos(item){

}

function updateAllItemPos(){
    pos = 1;
    var items = [];
    jQuery("canvas[type=page]").each(function(){
        jQuery(this).attr("pos", pos);
        var item_id = jQuery(this).attr("item_id");
        var item = {pos:pos, item_id:item_id};
        items.push(item);
        pos++;
    })

    jQuery.ajax({
        url: baseUrl+"ajax/update-items-pos",
        data:{items:items},
        type:"post",
        success:function(data){
            jQuery(".tab-pane-workarea").html(data.html);
            css_path = selected.next().attr("css_path");
            jQuery(".tab-pane-workarea").append("<link rel='stylesheet' href='"+css_path+"' />");
            jQuery(".btn-guardar-item").attr("disabled",false);
            jQuery(".audios-table tbody").html("");
        },
        error:function(data){

        },
    })
}

function getCountPages(){
    return jQuery("canvas[type=page]").length;
}