
jQuery(document).ready(function(){

    jQuery(document).on("click", ".selector-item-page", function (e) {
        e.preventDefault();
        active_item.id = jQuery(this).attr("item_id");
        active_item.item_selected = jQuery(this);
    });

    function fixCanvas(){

        w = parseInt(jQuery(".cropit-preview-image").css("width"))
        h = parseInt(jQuery(".cropit-preview-image").css("height"))
        t = jQuery(".cropit-preview-image").css("transform");
        tTemp = "";
        if(t.indexOf("matrix(")!==-1){
            tTemp = t.substring(7,t.length-1);
        }

        if(tTemp){
            tTemp = tTemp.split(" ");

            console.log(tTemp);

            scale = tTemp[0].substring(0,tTemp[0].length-1);
            left = tTemp[4];
            if(left.indexOf(",")!==-1){
                left = tTemp[4].substring(0,tTemp[4].length-1);
            }

            mtop = tTemp[5];
            if(mtop.indexOf(",")!==-1){
                mtop = tTemp[5].substring(0,tTemp[5].length-1);
            }

            translate = {
                scale:scale,
                left:left,
                top:mtop,
            }

            image_width = jQuery(".cropit-preview-image").css("width");
            image_height = jQuery(".cropit-preview-image").css("height");

            if(image_width){
                if(image_width.indexOf("px")!==-1){
                    image_width = image_width.substring(0,image_width.length-2);
                }
            }

            if(image_height){
                if(image_height.indexOf("px")!==-1){
                    image_height = image_height.substring(0,image_height.length-2);
                }
            }

            console.log("translate");
            console.log(translate);

            nw = w * eval(translate.scale);
            nh = h * eval(translate.scale);

            jQuery(".cropit-preview-image").css({
                "position":"absolute",
                "left":translate.left+"px",
                "top":translate.top+"px",
                "width":nw,
                "height":nh,
                "transform":"",
                "transform-origin" : "",
                "will-change": "",
            });
        }
    }

    jQuery(document).on("change", "cropit-image-input", function(){
        image_width = jQuery(".cropit-preview-image").css("width");
        image_height = jQuery(".cropit-preview-image").css("height");
        console.log(image_width);
        console.log(image_height);
    })

    jQuery(document).on("click",".btn-guardar-item",function(){

        transform = jQuery(".cropit-preview-image").css("transform");
        will_change = jQuery(".cropit-preview-image").css("will-change");
        transform_origin = jQuery(".cropit-preview-image").css("transform-origin");


        fixCanvas();

        if(!jQuery(".cropit-preview-image").attr("src")){
            $.alert("Seleccione una imagen");
            return false;
        }

        jQuery(".btn-guardar-item").attr("disabled",true);
        jQuery(".btn-add-item").attr("disabled",false);
        //jQuery(".btn-guardar-item").attr("disabled",false);
        $( '#image-label' ).attr( 'style', 'display:none' );
        $box_shadow = jQuery(".title-cover").css("box-shadow");
        jQuery(".title-cover").css({
            "background-color": "transparent",
            "border": "none",
            "box-shadow": "none"
        });

        saveOrUpdateItem();

        //loadItemTheme()

        setTimeout(function(){
            var canvas_selected = jQuery("canvas.selected");
            saveCanvas(canvas_selected.attr("item_id"));
        },2000);
    });

    jQuery(".btn-save-item").click(function(){
        jQuery(".btn-guardar-item").attr("disabled",true);
        jQuery(".btn-add-item").attr("disabled",false);
        jQuery('#modalItem button.close').trigger("click");
        loadItemTheme();

        selected_item = {
            type:"page",
            book_id:active_item.book_id,
            item_id:"",
        }

    });

    jQuery(document).on("click", '#modalItem button.close', function(){
        jQuery(".btn-guardar-item").attr("disabled",false);
    });

});