var transform, will_change, transform_origin, image_width, image_height;

function setBookActive(data){
    book_active.title = data.title;
    book_active.author_name = data.author_name;
    book_active.css = data.css;
    book_active.author_id = data.author_id;
    book_active.ilustrador_name=data.ilustrador_name;
}

function saveThumbnail(selector){
    //".panel-body-horizontal canvas"
    var imgData = jQuery(selector)[0].toDataURL('image/jpeg');

    $.ajax({
        type: "POST",
        url: baseUrl+"ajax/save-thumbnail",
        dataType: 'text',
        data: {
            base64data : imgData
        }
    });
}

function saveCanvas(item_id){
    canvas = jQuery("canvas[item_id="+item_id+"]");
    var data = canvas[0].toDataURL("image/png");
    $.post(baseUrl+"ajax/save-thumbnail", {book_id:current_user, item_id:item_id, data: canvas[0].toDataURL("image/png")}).done(function( data ) {
        data = JSON.parse(data);
        book_active.thumnail = data.path;
        var r = canvas.attr("resized") || false;
        if(selected_item.item_id != item_id){
            jQuery(".cropit-preview-image").css("transform", transform);
            jQuery(".cropit-preview-image").css("will-change", will_change);
            jQuery(".cropit-preview-image").css("transform-origin", transform_origin);
            jQuery(".cropit-preview-image").css("position", "static");
            jQuery(".cropit-preview-image").css("left", "auto");
            jQuery(".cropit-preview-image").css("top", "auto");
            jQuery(".cropit-preview-image").css("width", image_width+"px");
            jQuery(".cropit-preview-image").css("height", image_height+"px");
            canvas.attr("resized", true);
        }
		$(".overlay").hide();
    });
}