<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/home', 'HomeController@index')->middleware('roles:*');

    Route::get('admin', 'AdminController@index')->middleware('roles:Admin');
    Route::get('admin/theme_wizzard', 'ThemeWizzardController@index')->middleware('roles:Admin');

    //Auth::routes();

    // Login Routes...
    Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
    Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

    // Registration Routes...
    Route::get('register', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
    Route::post('register', ['as' => 'register.post', 'uses' => 'Auth\RegisterController@register']);

    // Password Reset Routes...
    Route::get('password/reset', ['as' => 'password.reset', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
    Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
    Route::get('password/reset/{token}', ['as' => 'password.reset.token', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
    Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\ResetPasswordController@reset']);

    //activation email
    Route::get('user/activation/{token}', 'Auth\RegisterController@activateUser')->name('user.activate');

    Route::get('lang/{lang}', function ($lang) {
        session(['custom_lang' => $lang]);
        return \Redirect::back();
    })->where([
        'lang' => 'en|es'
    ]);

    Route::get('/editor/{book_id?}', 'EditorController@index');
    Route::get('/ver/{book_id?}', 'ViewerController@index');
    Route::post('/ajax', 'AjaxController@index');
    //Route::get('/ajax/saveItem', 'AjaxController@index');
    Route::post('ajax/save-or-update-item',   ['uses'=>'AjaxController@saveOrUpdateItem']);
    Route::post('ajax/upload-pre-audio',   ['uses'=>'AjaxController@uploadPreAudio']);
    Route::post('ajax/save-or-update-audio',   ['uses'=>'AjaxController@saveOrUpdateAudio']);
    Route::post('ajax/delete-book',   ['uses'=>'AjaxController@deleteBook']);
    Route::post('ajax/load-html-item-saved',   ['uses'=>'AjaxController@loadHtmlItemSaved']);
    Route::post('ajax/save-or-update-book',   ['uses'=>'AjaxController@saveOrUpdateBook']);
    Route::post('ajax/delete-item',   ['uses'=>'AjaxController@deleteItem']);
    Route::post('ajax/update-items-pos',   ['uses'=>'AjaxController@updateItemsPos']);
    Route::post('ajax/save-thumbnail',   ['uses'=>'AjaxController@saveThumbnail']);
    Route::post('ajax/make-apk',   ['uses'=>'AjaxController@makeApk']);


    Route::get('/clear-cache', function() {
        $exitCode = Artisan::call('cache:clear');
        // return what you want
    });

    Route::get('/key:generate', function() {
        $exitCode = Artisan::call('key:generate');
        // return what you want
    });


});

