<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name','Admin')->first();
        $role_user = Role::where('name','User')->first();
        $this->testUser($role_admin);
        $this->testAdmin($role_user);
    }

    private function testUser($role){
        $user = new \App\Models\User();
        $user->name = "Name";
        $user->surname = "Surname";
        $user->username = "DaAdmin";
        $user->email = "whatever@hotmail.com";
        $user->password = bcrypt('Test12345!');
        $user->activated = true;
        $user->save();
        $user->roles()->attach($role);
    }

    private function testAdmin($role){
        $user = new \App\Models\User();
        $user->name = "Juan";
        $user->surname = "Juan";
        $user->username = "jotapey";
        $user->email = "juanbarla@gmail.com";
        $user->password = bcrypt('q1w2e3r4');
        $user->activated = true;
        $user->save();
        $user->roles()->attach($role);
    }
}
